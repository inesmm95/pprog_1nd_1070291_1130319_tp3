package isep.pprog.ce;


import isep.pprog.ce.dominio.CentroDeEventos;
import isep.pprog.ce.dominio.Utilizador;
import isep.pprog.ce.servico.ServicoPersistencia;
import isep.pprog.ce.servico.ServicoPersistenciaFicheiroJson;
import isep.pprog.ce.uc.AtribuirCandidaturaUC;
import isep.pprog.ce.uc.CentroDeEventosUC;
import isep.pprog.ce.uc.CriarCandidaturaUC;
import isep.pprog.ce.uc.DecidirCandidaturaUC;
import isep.pprog.ce.util.InterfaceTerminalHelper;
import isep.pprog.ce.util.MensagemTerminal;

/**
 * Executável da aplicação "Centro de Eventos"
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public class AplicacaoCentroDeEventos {

    private static ServicoPersistencia servicoPersistencia = new ServicoPersistenciaFicheiroJson();

    public static void main(String[] args) {

        CentroDeEventos centroDeEventos = servicoPersistencia.lerCentroDeEventos();

        InterfaceTerminalHelper.imprimir(MensagemTerminal.BEM_VINDO_CENTRO_EVENTOS);

        Utilizador utilizador = login(centroDeEventos);

        menu(centroDeEventos, utilizador);

    }

    /**
     * Executa login com nome de utilizador
     *
     * @param centroDeEventos Centro de Eventoos
     */
    private static Utilizador login(CentroDeEventos centroDeEventos) {

        InterfaceTerminalHelper.imprimir(MensagemTerminal.LOGIN_COM_NOME_UTILIZADOR);

        Utilizador utilizador = null;

        //Loop infinito enquanto utilizador não estiver identificado no sistema
        while (utilizador == null) {

            String nomeDeUtilizador = InterfaceTerminalHelper.lerString();

            utilizador = centroDeEventos.obterUtilizadorByNomeDeUtilizador(nomeDeUtilizador);

            if (utilizador == null) {
                InterfaceTerminalHelper.imprimir(MensagemTerminal.UTILIZADOR_NAO_EXISTE_INSIRA_NOV);
            }

        }

        return utilizador;
    }

    private static void menu(CentroDeEventos centroDeEventos, Utilizador utilizador) {

        while (true) {

            InterfaceTerminalHelper.imprimir(MensagemTerminal.INSIRA_A_ACCAO_PRETENDIDA);
            InterfaceTerminalHelper.imprimir(MensagemTerminal.ATRIBUIR_CANDIDATURA);
            InterfaceTerminalHelper.imprimir(MensagemTerminal.DECIDIR_CANDIDATURA);
            InterfaceTerminalHelper.imprimir(MensagemTerminal.CRIAR_CANDIDATURA);
            InterfaceTerminalHelper.imprimir(MensagemTerminal.LOGOUT);
            InterfaceTerminalHelper.imprimir(MensagemTerminal.SAIR);

            int opcaoEscolhida = InterfaceTerminalHelper.lerInt();

            switch (opcaoEscolhida) {
                case 1:
                    CentroDeEventosUC atribuirCandidaturaUC = new AtribuirCandidaturaUC(centroDeEventos, utilizador);
                    validaAcessoEExecutaUC(atribuirCandidaturaUC);
                    servicoPersistencia.escreverCentroDeEventos(centroDeEventos);
                    break;
                case 2:
                    CentroDeEventosUC decidirCandidatura = new DecidirCandidaturaUC(centroDeEventos, utilizador);
                    validaAcessoEExecutaUC(decidirCandidatura);
                    servicoPersistencia.escreverCentroDeEventos(centroDeEventos);
                    break;
                case 3:
                    CentroDeEventosUC criarCandidaturaUC = new CriarCandidaturaUC(centroDeEventos, utilizador);
                    validaAcessoEExecutaUC(criarCandidaturaUC);
                    servicoPersistencia.escreverCentroDeEventos(centroDeEventos);
                    break;
                case 0:
                    //Chamada recursiva ::login
                    utilizador = login(centroDeEventos);
                    break;
                case 9:
                    System.exit(0);
                    break;
                default:
                    InterfaceTerminalHelper.imprimir(MensagemTerminal.OPCAO_INVALIDA);
            }
        }
    }

    /**
     * Valida o acesso por tipo de utilizador e executa UC
     *
     * @param centroDeEventosUC UC do Centro de Eventos
     */
    private static void validaAcessoEExecutaUC(CentroDeEventosUC centroDeEventosUC) {

        CentroDeEventos centroDeEventos = centroDeEventosUC.getCentroDeEventos();
        Utilizador utilizadorAutenticado = centroDeEventosUC.getUtilizadorAutenticado();

        if (centroDeEventosUC instanceof AtribuirCandidaturaUC && centroDeEventos.isOrganizador(utilizadorAutenticado)) {
            centroDeEventosUC.executarUC();

        } else if (centroDeEventosUC instanceof DecidirCandidaturaUC && centroDeEventos.isFuncApoioEvento(utilizadorAutenticado)) {
            centroDeEventosUC.executarUC();

        } else if (centroDeEventosUC instanceof CriarCandidaturaUC && centroDeEventos.isRepresentante(utilizadorAutenticado)) {
            centroDeEventosUC.executarUC();

        } else {
            InterfaceTerminalHelper.imprimir(MensagemTerminal.TIPO_UTILIZADOR_SEM_PERMISSAO);
        }
    }
    
}
