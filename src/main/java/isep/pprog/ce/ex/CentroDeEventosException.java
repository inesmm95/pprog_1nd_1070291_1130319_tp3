package isep.pprog.ce.ex;

/**
 * Excepção genérica para a aplicação Centro de Eventos
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public class CentroDeEventosException extends RuntimeException {

    public CentroDeEventosException(String message) {
        super(message);
    }
}
