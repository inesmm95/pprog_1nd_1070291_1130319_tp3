package isep.pprog.ce.ex;

/**
 * Excepção a lançar quando há algum erro de instanciação ou execução
 * de um algoritmo de atribuição
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public class AlgorimoAtribuicaoException extends CentroDeEventosException {

    /**
     *
     * @param message Mensagem de erro
     */
    public AlgorimoAtribuicaoException(String message) {
        super(message);
    }
}
