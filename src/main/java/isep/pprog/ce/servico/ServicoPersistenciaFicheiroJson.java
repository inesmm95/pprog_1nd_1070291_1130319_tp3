package isep.pprog.ce.servico;

import com.fasterxml.jackson.databind.ObjectMapper;
import isep.pprog.ce.dominio.CentroDeEventos;

import java.io.*;

/**
 * Implementação das funcionalidades de leitura e escrita de dados da
 * aplicação para ficheiro
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public class ServicoPersistenciaFicheiroJson implements ServicoPersistencia {

    private ObjectMapper objectMapper = new ObjectMapper();

    private String nomeDoFicheiro;

    private static final String NOME_FICHEIRO_OMISSAO = "centro_eventos.json";

    /**
     * @param nomeDoFicheiro Nome do ficheiro a ler/escrever
     */
    public ServicoPersistenciaFicheiroJson(String nomeDoFicheiro) {
        this.nomeDoFicheiro = nomeDoFicheiro;
    }

    public ServicoPersistenciaFicheiroJson() {
        this.nomeDoFicheiro = NOME_FICHEIRO_OMISSAO;
    }

    /**
     * Lê CentroDeEventos do ficheiro
     *
     * @return CentroDeEventos
     */
    @Override
    public CentroDeEventos lerCentroDeEventos() {
        //init();
        try (Reader reader = new FileReader(nomeDoFicheiro)) {
            return objectMapper.readValue(reader, CentroDeEventos.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Escreve CentroDeEventos para ficheiro
     *
     */
    @Override
    public void escreverCentroDeEventos(CentroDeEventos centroDeEventos) {
        try (Writer writer = new FileWriter(nomeDoFicheiro)) {
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(writer, centroDeEventos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
