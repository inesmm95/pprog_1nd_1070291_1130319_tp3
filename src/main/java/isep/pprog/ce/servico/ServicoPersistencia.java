package isep.pprog.ce.servico;


import isep.pprog.ce.dominio.CentroDeEventos;

/**
 * Interface de leitura e escrita de dados da aplicação
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public interface ServicoPersistencia {

    /**
     * Lê CentroDeEventos
     *
     * @return CentroDeEventos
     */
    CentroDeEventos lerCentroDeEventos();

    /**
     * Escreve CentroDeEventos
     *
     * @param centroDeEventos CentroDeEventos
     */
    void escreverCentroDeEventos(CentroDeEventos centroDeEventos);

}
