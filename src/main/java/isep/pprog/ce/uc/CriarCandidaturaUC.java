package isep.pprog.ce.uc;


import isep.pprog.ce.dominio.*;
import isep.pprog.ce.util.InterfaceTerminalHelper;
import isep.pprog.ce.util.MensagemTerminal;

import java.util.List;

/**
 * Classe que implementa a lógica de criação de candidaturas
 * e interação com o utilizador
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public class CriarCandidaturaUC extends CentroDeEventosUC {

    public CriarCandidaturaUC(CentroDeEventos centroDeEventos, Utilizador utilizadorAutenticado) {
        super(centroDeEventos, utilizadorAutenticado);
    }

    @Override
    public void executarUC() {

        CentroDeEventos centroDeEventos = super.getCentroDeEventos();
        Utilizador utilizadorAutenticado = super.getUtilizadorAutenticado();


        List<Evento> eventosAbertosACandidaturas = centroDeEventos.getEventos();

        if (eventosAbertosACandidaturas.isEmpty()) {
            InterfaceTerminalHelper.imprimir(MensagemTerminal.NAO_HA_EVENTOS_ABERTOS_A_CANDIDATURAS);
            return;
        }

        Evento evento = seleccionarEvento(eventosAbertosACandidaturas);

        Representante representante = centroDeEventos.obterRepresentanteByUtilizador(utilizadorAutenticado);

        Candidatura candidatura = solicitaDadosECriaCandidatura(evento, representante);

        InterfaceTerminalHelper.imprimir(String.format("%s - %s", MensagemTerminal.CONFIRMA_CRIACAO_CANDIDATURA.getMensagem(), candidatura));

        boolean candidaturaConfirmada = false;

        while (!candidaturaConfirmada) {

            int opcaoConfirmacao = InterfaceTerminalHelper.lerInt();

            if (opcaoConfirmacao == InterfaceTerminalHelper.UM) {
                evento.getCandidaturas().add(candidatura);
                InterfaceTerminalHelper.imprimir(MensagemTerminal.CANDIDATURA_SUBMETIDA_COM_SUCESSO);
                candidaturaConfirmada = true;

            } else if (opcaoConfirmacao == InterfaceTerminalHelper.ZERO) {
                InterfaceTerminalHelper.imprimir(MensagemTerminal.CANDIDATURA_NAO_SUBMETIDA);
                candidaturaConfirmada = true;

            } else {
                InterfaceTerminalHelper.imprimir(MensagemTerminal.OPCAO_INVALIDA);
            }
        }

    }

    private Evento seleccionarEvento(List<Evento> eventosAbertosACandidaturas) {
        InterfaceTerminalHelper.imprimir(MensagemTerminal.SELECCIONE_EVENTO);

        for (int i = 0; i < eventosAbertosACandidaturas.size(); i++) {
            InterfaceTerminalHelper.imprimir(String.format("%d - %s", (i + 1), eventosAbertosACandidaturas.get(i)));
        }

        while (true) {
            int eventoSeleccionado = InterfaceTerminalHelper.lerInt();
            try {
                return eventosAbertosACandidaturas.get(eventoSeleccionado - 1);
            } catch (Exception e) {
                InterfaceTerminalHelper.imprimir(MensagemTerminal.EVENTO_INVALIDO);
            }
        }
    }

    private Candidatura solicitaDadosECriaCandidatura(Evento evento, Representante representante) {

        InterfaceTerminalHelper.imprimir(MensagemTerminal.INSIRA_DESCICAO_CANDIDATURA);
        final String descricao = InterfaceTerminalHelper.lerString();

        Candidatura candidatura = new Candidatura();
        candidatura.setEvento(evento);
        candidatura.setRepresentante(representante);
        candidatura.setDescricao(descricao);

        return candidatura;
    }

}
