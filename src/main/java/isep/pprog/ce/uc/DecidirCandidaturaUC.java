package isep.pprog.ce.uc;

import isep.pprog.ce.dominio.*;
import isep.pprog.ce.util.InterfaceTerminalHelper;
import isep.pprog.ce.util.MensagemTerminal;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe que implementa a lógica de decisão de candidaturas
 * e interação com o utilizador
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public class DecidirCandidaturaUC extends CentroDeEventosUC {

    public DecidirCandidaturaUC(CentroDeEventos centroDeEventos, Utilizador utilizadorAutenticado) {
        super(centroDeEventos, utilizadorAutenticado);
    }

    public void executarUC() {

        CentroDeEventos centroDeEventos = super.getCentroDeEventos();
        Utilizador utilizadorAutenticado = super.getUtilizadorAutenticado();

        FuncApoioEvento funcApoioEvento = centroDeEventos.obterFuncApoioEventoByUtilizador(utilizadorAutenticado);

        List<Candidatura> candidaturasAtribuidasAoFuncApoioEvento = new ArrayList<>();
        for (Evento evento : centroDeEventos.getEventos()) {
            for (Atribuicao atribuicao : evento.getAtribuicoes()) {
                if (atribuicao.getFuncApoioEvento().equals(funcApoioEvento)) {
                    candidaturasAtribuidasAoFuncApoioEvento.add(atribuicao.getCandidatura());
                }
            }
        }

        if (!funcApoioEventoTemCandidaturas(candidaturasAtribuidasAoFuncApoioEvento)) {
            return;
        }

        Candidatura candidatura = seleccionarCandidatura(candidaturasAtribuidasAoFuncApoioEvento);

        Decisao decisao = obterDecisao();

        confirmarDecisao(candidatura, decisao);

    }

    private void confirmarDecisao(Candidatura candidatura, Decisao decisao) {

        InterfaceTerminalHelper.imprimir(decisao.toString());

        InterfaceTerminalHelper.imprimir(MensagemTerminal.CONFIRMA_DECISAO);

        boolean opcaoNaoConfirmada = true;

        do {

            int opcaoConfirmacao = InterfaceTerminalHelper.lerInt();

            if (opcaoConfirmacao == InterfaceTerminalHelper.UM) {
                candidatura.setDecisao(decisao);
                InterfaceTerminalHelper.imprimir(MensagemTerminal.A_DECISAO_FOI_CONFIRMADA);
                opcaoNaoConfirmada = false;

            } else if (opcaoConfirmacao == InterfaceTerminalHelper.ZERO) {
                InterfaceTerminalHelper.imprimir(MensagemTerminal.A_DECISAO_NAO_FOI_CONFIRMADA);
                opcaoNaoConfirmada = false;

            } else {
                InterfaceTerminalHelper.imprimir(MensagemTerminal.OPCAO_INVALIDA);
            }

        } while (opcaoNaoConfirmada);

    }

    private boolean funcApoioEventoTemCandidaturas(List<Candidatura> candidaturasAtribuidasAoFuncApoioEvento) {

        if (!candidaturasAtribuidasAoFuncApoioEvento.isEmpty()) {
            return true;
        }
        InterfaceTerminalHelper.imprimir(MensagemTerminal.NAO_POSSUI_CANDIDATURAS_PENDENTES);
        return false;
    }

    private Candidatura seleccionarCandidatura(List<Candidatura> candidaturasAtribuidasAoFuncApoioEvento) {

        while (true) {

            InterfaceTerminalHelper.imprimir(MensagemTerminal.SELECCIONE_UMA_CANDIDATURA);

            for (int i = 0; i < candidaturasAtribuidasAoFuncApoioEvento.size(); i++) {
                InterfaceTerminalHelper.imprimir((i + 1) + " - " + candidaturasAtribuidasAoFuncApoioEvento.get(i));
            }

            int candidaturaSeleccionada = InterfaceTerminalHelper.lerInt();

            try {
                return candidaturasAtribuidasAoFuncApoioEvento.get(candidaturaSeleccionada - 1);
            } catch (Exception e) {
                InterfaceTerminalHelper.imprimir(MensagemTerminal.A_CANDIDATURA_NAO_EXISTE);
            }
        }
    }

    private Decisao obterDecisao() {

        Decisao decisao = new Decisao();

        boolean decisaoPorTomar = true;

        while (decisaoPorTomar) {

            InterfaceTerminalHelper.imprimir(MensagemTerminal.ACEITA_CANDIDATURA);

            int opcaoConfirmacao = InterfaceTerminalHelper.lerInt();

            if (opcaoConfirmacao == InterfaceTerminalHelper.UM) {
                decisao.setAceite(true);
                decisaoPorTomar = false;

            } else if (opcaoConfirmacao == InterfaceTerminalHelper.ZERO) {
                decisao.setAceite(false);
                decisaoPorTomar = false;

            } else {
                InterfaceTerminalHelper.imprimir(MensagemTerminal.OPCAO_INVALIDA);
            }
        }

        InterfaceTerminalHelper.imprimir(MensagemTerminal.JUSTIFIQUE_DECISAO);
        decisao.setTextoJustificativo(InterfaceTerminalHelper.lerString());

        return decisao;

    }

}
