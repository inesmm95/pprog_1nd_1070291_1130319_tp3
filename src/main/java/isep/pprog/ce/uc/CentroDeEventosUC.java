package isep.pprog.ce.uc;

import isep.pprog.ce.dominio.CentroDeEventos;
import isep.pprog.ce.dominio.Utilizador;

import java.util.Objects;

/**
 * A classe {@code CentroDeEventosUC} é a superclass de qualquer implementação
 * (subclass) de classes específicas de UC.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public abstract class CentroDeEventosUC {

    private CentroDeEventos centroDeEventos;
    private Utilizador utilizadorAutenticado;

    public CentroDeEventosUC(CentroDeEventos centroDeEventos,
                                    Utilizador utilizadorAutenticado) {
        this.centroDeEventos = centroDeEventos;
        this.utilizadorAutenticado = utilizadorAutenticado;
    }

    public CentroDeEventos getCentroDeEventos() {
        return centroDeEventos;
    }

    public void setCentroDeEventos(CentroDeEventos centroDeEventos) {
        this.centroDeEventos = centroDeEventos;
    }

    public Utilizador getUtilizadorAutenticado() {
        return utilizadorAutenticado;
    }

    public void setUtilizadorAutenticado(Utilizador utilizadorAutenticado) {
        this.utilizadorAutenticado = utilizadorAutenticado;
    }

    public abstract void executarUC();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CentroDeEventosUC)) {
            return false;
        }
        CentroDeEventosUC that = (CentroDeEventosUC) o;
        return Objects.equals(centroDeEventos, that.centroDeEventos) &&
                Objects.equals(utilizadorAutenticado, that.utilizadorAutenticado);
    }

    @Override
    public int hashCode() {
        return Objects.hash(centroDeEventos, utilizadorAutenticado);
    }

    @Override
    public String toString() {
        return String.format(
                "CentroDeEventosUC [centroDeEventos=%s, utilizadorAutenticado=%s]",
                this.centroDeEventos,
                this.utilizadorAutenticado);
    }
}
