package isep.pprog.ce.uc;


import isep.pprog.ce.dominio.*;
import isep.pprog.ce.dominio.algoritmos.AlgoritmoAtribuicaoCandidaturas;
import isep.pprog.ce.dominio.algoritmos.AlgoritmoDistrubuicaoEquitativaFaes;
import isep.pprog.ce.dominio.algoritmos.AlgoritmoExperienciaProfissionalFaes;
import isep.pprog.ce.dominio.algoritmos.AlgoritmoFaesPorCandidatura;
import isep.pprog.ce.ex.AlgorimoAtribuicaoException;
import isep.pprog.ce.util.InterfaceTerminalHelper;
import isep.pprog.ce.util.MensagemTerminal;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe que implementa a lógica de atribuição de candidaturas
 * e interação com o utilizador
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public class AtribuirCandidaturaUC extends CentroDeEventosUC {

    /**
     * @param centroDeEventos       Centro de Eventos
     * @param utilizadorAutenticado Uilizador autenticado
     */
    public AtribuirCandidaturaUC(CentroDeEventos centroDeEventos, Utilizador utilizadorAutenticado) {
        super(centroDeEventos, utilizadorAutenticado);
    }

    @Override
    public void executarUC() {

        CentroDeEventos centroDeEventos = super.getCentroDeEventos();
        Utilizador utilizadorAutenticado = super.getUtilizadorAutenticado();

        Organizador organizador = centroDeEventos.obterOrganizadorByUtilizador(utilizadorAutenticado);

        List<Evento> eventosDoOrganizador = organizador.getEventos();

        if (!organizadorTemEventos(eventosDoOrganizador)) {
            return;
        }

        Evento evento = seleccionarEvento(eventosDoOrganizador);

        if (!eventoTemFaes(evento)) {
            return;
        }

        if (!eventoTemCandidaturas(evento)) {
            return;
        }

        //limpa as atribuições já realizadas
        evento.setAtribuicoes(new ArrayList<>());

        while (evento.getAtribuicoes().isEmpty()) {

            AlgoritmoAtribuicaoCandidaturas algoritmoAtribuicaoCandidaturas = seleccionarAlgoritmo(evento, organizador);

            List<Atribuicao> atribuicoes = algoritmoAtribuicaoCandidaturas.atribui();

            boolean atribuicoesAceites = aceitaOuRejeitaAtribuicoes(atribuicoes);

            if (atribuicoesAceites) {
                evento.setAtribuicoes(atribuicoes);
            }

        }

        InterfaceTerminalHelper.imprimir(MensagemTerminal.ATRIBUICOES_GUARDADAS);

    }

    private boolean aceitaOuRejeitaAtribuicoes(List<Atribuicao> atribuicoes) {

        InterfaceTerminalHelper.imprimir(MensagemTerminal.ACEITA_AS_ATRIBUICOES);

        for (Atribuicao atribuicao: atribuicoes) {
            InterfaceTerminalHelper.imprimir(atribuicao.toString());
        }

        while (true) {
            int opcaoConfirmacao = InterfaceTerminalHelper.lerInt();
            if (opcaoConfirmacao == InterfaceTerminalHelper.UM) {
                return true;
            } else if (opcaoConfirmacao == InterfaceTerminalHelper.ZERO) {
                return false;
            } else {
                InterfaceTerminalHelper.imprimir(MensagemTerminal.OPCAO_INVALIDA);
            }
        }

    }

    private AlgoritmoAtribuicaoCandidaturas seleccionarAlgoritmo(Evento evento, Organizador organizador) {

        InterfaceTerminalHelper.imprimir(MensagemTerminal.SELECCIONE_ALGORITMO_PRETENDIDO);
        InterfaceTerminalHelper.imprimir(MensagemTerminal.UM.getMensagem() + AlgoritmoDistrubuicaoEquitativaFaes.NOME_DO_ALGORITMO);
        InterfaceTerminalHelper.imprimir(MensagemTerminal.DOIS.getMensagem() + AlgoritmoExperienciaProfissionalFaes.NOME_DO_ALGORITMO);
        InterfaceTerminalHelper.imprimir(MensagemTerminal.TRES.getMensagem() + AlgoritmoFaesPorCandidatura.NOME_DO_ALGORITMO);

        boolean algoritmoInvalido = true;

        int opcaoSeleccionada = Integer.MAX_VALUE;

        while (algoritmoInvalido) {
            opcaoSeleccionada = InterfaceTerminalHelper.lerInt();
            if (opcaoSeleccionada < InterfaceTerminalHelper.UM || opcaoSeleccionada > InterfaceTerminalHelper.TRES) {
                InterfaceTerminalHelper.imprimir(MensagemTerminal.ALGORITMO_INVALIDO);
            } else {
                algoritmoInvalido = false;
            }
        }

        switch (opcaoSeleccionada) {

            case InterfaceTerminalHelper.UM:
                try {
                    return new AlgoritmoDistrubuicaoEquitativaFaes(evento, organizador);
                } catch (AlgorimoAtribuicaoException e) {
                    InterfaceTerminalHelper.imprimir(e.getMessage());
                    return seleccionarAlgoritmo(evento, organizador);
                }
            case InterfaceTerminalHelper.DOIS:
                try {
                    return new AlgoritmoExperienciaProfissionalFaes(evento, organizador);
                } catch (AlgorimoAtribuicaoException e) {
                    InterfaceTerminalHelper.imprimir(e.getMessage());
                    return seleccionarAlgoritmo(evento, organizador);
                }
            case InterfaceTerminalHelper.TRES:
                InterfaceTerminalHelper.imprimir(
                        MensagemTerminal.SELECCIONE_NR_FAES_CANDIDATURA.getMensagem()
                                + " Nr. FAEs = " + evento.getFuncsApoioEvento().size()
                                + " Nr. Candidaturas = " + evento.getCandidaturas().size()
                        );
                int numeroDeFaesCandidatura = InterfaceTerminalHelper.lerInt();
                try {
                    return new AlgoritmoFaesPorCandidatura(numeroDeFaesCandidatura, evento, organizador);
                } catch (AlgorimoAtribuicaoException e) {
                    InterfaceTerminalHelper.imprimir(e.getMessage());
                    return seleccionarAlgoritmo(evento, organizador);
                }
            default:
                throw new IllegalStateException(MensagemTerminal.OCORREU_UM_ERRO_NA_SELECCAO_ALGORITMO.toString());
        }
    }

    private boolean organizadorTemEventos(List<Evento> eventosDoOrganizador) {
        if (!eventosDoOrganizador.isEmpty()) {
            return true;
        }
        InterfaceTerminalHelper.imprimir(MensagemTerminal.NAO_TEM_EVENTOS_SELECCIONADOS);
        return false;
    }

    private boolean eventoTemFaes(Evento evento) {
        if (!evento.getFuncsApoioEvento().isEmpty()) {
            return true;
        }
        InterfaceTerminalHelper.imprimir(MensagemTerminal.EVENTO_NAO_TEM_FAES);
        return false;
    }

    private boolean eventoTemCandidaturas(Evento evento) {
        if (!evento.getCandidaturas().isEmpty()) {
            return true;
        }
        InterfaceTerminalHelper.imprimir(MensagemTerminal.EVENTO_NAO_TEM_CANDIDATURAS);
        return false;
    }

    private Evento seleccionarEvento(List<Evento> eventosOrganizador) {

        while (true) {

            InterfaceTerminalHelper.imprimir(MensagemTerminal.SELECCIONE_EVENTO);

            for (int i = 0; i < eventosOrganizador.size(); i++) {
                InterfaceTerminalHelper.imprimir(
                        String.format("%d - %s", (i + 1), eventosOrganizador.get(i))
                );
            }

            int eventoSeleccionado = InterfaceTerminalHelper.lerInt();

            try {
                return eventosOrganizador.get(eventoSeleccionado - InterfaceTerminalHelper.UM);
            } catch (Exception e) {
                InterfaceTerminalHelper.imprimir(MensagemTerminal.EVENTO_NAO_EXISTE);
            }

        }
    }

}
