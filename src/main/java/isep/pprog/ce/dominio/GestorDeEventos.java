package isep.pprog.ce.dominio;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Objects;

/**
 * A classe {@code GestorDeEventos} representa um gestor de eventos.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class GestorDeEventos extends ClasseDominio {
    
    /** Utilizador que tem papel de gestor de eventos */
    private Utilizador utilizador;

    public GestorDeEventos() {
    }

    /**
     *
     * @param utilizador     Utilizador que tem papel de gestor de eventos  
     */ 
    public GestorDeEventos(Utilizador utilizador) {
        this.utilizador = new Utilizador(utilizador);
    }

    /**
     * @return Utilizador que tem papel de gestor de eventos
     */
    public Utilizador getUtilizador() {
        return utilizador;
    }

    /**
     * Define Utilizador que tem papel de gestor de eventos
     *
     * @param utilizador que tem papel de gestor de eventos
     */
    public void setUtilizador(Utilizador utilizador) {
        this.utilizador = utilizador;
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof GestorDeEventos)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        GestorDeEventos that = (GestorDeEventos) o;
        return Objects.equals(utilizador, that.utilizador);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(utilizador);
    }

    /**
     * @return GestorDeEventos alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "GestorDeEventos [id=%s, utilizador=%s]",
                super.toString(),
                this.utilizador != null ? this.utilizador.getNome() : NA);
    }
}
