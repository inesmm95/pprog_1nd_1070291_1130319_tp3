package isep.pprog.ce.dominio;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Objects;

/**
 * A classe {@code Utilizador} representa um utilizador do sistema.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Utilizador extends ClasseDominio {
    
    /** Nome do utilizador */
    private String nome;
    
    /** Email do utilizador */
    private String email;
    
    /** Username do utilizador */
    private String username;
    
    /** Password do utilizador */
    private String passwordEncriptada;
    
    /** Nome do utilizador por omissão  */
    private static final String NOME_POR_OMISSAO = "sem nome";
    
    /** Username do utilizador por omissão */
    private static final String USERNAME_POR_OMISSAO = "user";
    
    /** Password do utilizador por omissão */
    private static final String PASSWORD_POR_OMISSAO = "sem password";

    public Utilizador() {
        this.nome = NOME_POR_OMISSAO;
        this.username = USERNAME_POR_OMISSAO;
        this.passwordEncriptada = PASSWORD_POR_OMISSAO;
    }

    /**
     *
     * @param nome               Nome do utilizador
     * @param email              Email do utilizador
     * @param username           Username do utilizador
     * @param passwordEncriptada Password do utilizador
   */  
    public Utilizador(String nome, String email, String username, String passwordEncriptada) {
        this.nome = nome;
        this.email = email;
        this.username = username;
        this.passwordEncriptada = passwordEncriptada;
    }
    /**
     *
     * @param utilizador  Utilizador do sistema
   */  
    public Utilizador(Utilizador utilizador) {
        this.nome = utilizador.getNome();
        this.email = utilizador.getEmail();
        this.username = utilizador.getUsername();
        this.passwordEncriptada = utilizador.getPasswordEncriptada();
    }

    /**
     * @return Nome do utilizador
     */
    public String getNome() {
        return nome;
    }

    /**
     * Define Nome do utilizador
     *
     * @param nome do utilizador
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return Email do utilizador
     */
    public String getEmail() {
        return email;
    }

    /**
     * Define Email do utilizador
     *
     * @param email do utilizador
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return Username do utilizador
     */
    public String getUsername() {
        return username;
    }

    /**
     * Define Username do utilizador
     *
     * @param username do utilizador
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return Password do utilizador
     */
    public String getPasswordEncriptada() {
        return passwordEncriptada;
    }

    /**
     * Define Password do utilizador
     *
     * @param passwordEncriptada do utilizador
     */
    public void setPasswordEncriptada(String passwordEncriptada) {
        this.passwordEncriptada = passwordEncriptada;
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Utilizador)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Utilizador that = (Utilizador) o;
        return Objects.equals(nome, that.nome) &&
                Objects.equals(email, that.email) &&
                Objects.equals(username, that.username) &&
                Objects.equals(passwordEncriptada, that.passwordEncriptada);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(nome, email, username, passwordEncriptada);
    }

    /**
     * @return Utilizador alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "Utilizador [id=%s, nome=%s, email=%s, username=%s, passwordEncriptada=%s]",
                super.toString(),
                this.nome,
                this.email,
                this.username,
                this.passwordEncriptada);
    }
}
