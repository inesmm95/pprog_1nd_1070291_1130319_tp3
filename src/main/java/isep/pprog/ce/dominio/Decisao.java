package isep.pprog.ce.dominio;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Objects;

/**
 * A classe {@code Decisao} representa as decisões dos FAE pelas candidaturas.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Decisao extends ClasseDominio {
    
    /** decisao da candidatura: aceita=true; rejeita=false */
    private boolean aceite;
    
    /** Texto justificativo da decisão*/
    private String textoJustificativo;
    
    /** Decisao por omissão */
    private static final boolean ACEITE_POR_OMISSAO = false;
    
    /** Texto justificativo por omisssão da decisão */
    private static final String TEXTO_JUSTIFICATIVO_POR_OMISSAO = "sem justificação";

    public Decisao() {
        this.aceite = ACEITE_POR_OMISSAO;
        this.textoJustificativo = TEXTO_JUSTIFICATIVO_POR_OMISSAO;
    }

    /**
     *
     * @param aceite               Decisao da candidatura
     * @param textoJustificativo   Texto justificativo da decisão
     */   
    public Decisao(boolean aceite, String textoJustificativo) {
        this.aceite = aceite;
        this.textoJustificativo = textoJustificativo;
    }

    /**
     * @return {@code true} se a decisão for aceite, {@code false} caso contrário
     */
    public boolean isAceite() {
        return aceite;
    }

    /**
     * Define a decisão da candidatura
     *
     * @param aceite da candidatura
     */
    public void setAceite(boolean aceite) {
        this.aceite = aceite;
    }

    /**
     * @return o texto justificativo da decisão
     */
    public String getTextoJustificativo() {
        return textoJustificativo;
    }

    /**
     * Define o texto justificativo da decisão
     *
     * @param textoJustificativo da decisão
     */
    public void setTextoJustificativo(String textoJustificativo) {
        this.textoJustificativo = textoJustificativo;
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Decisao)) {
            return false;
        }
        Decisao decisao = (Decisao) o;
        return aceite == decisao.aceite &&
                Objects.equals(textoJustificativo, decisao.textoJustificativo);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), aceite, textoJustificativo);
    }

    /**
     * @return Decisao alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "Decisao [id=%s, aceite=%s, textoJustificativo=%s]",
                super.toString(),
                this.aceite,
                this.textoJustificativo);
    }
}
