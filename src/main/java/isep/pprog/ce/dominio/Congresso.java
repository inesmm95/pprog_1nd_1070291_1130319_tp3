package isep.pprog.ce.dominio;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * A classe {@code Congresso} representa um evento do tipo Congresso.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
@JsonDeserialize(as = Congresso.class)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Congresso extends Evento {
    
    /** Lista de workshops adicionados ao congresso */
    @JsonIgnoreProperties
    private List<Workshop> workshops;
    
    /** Lista de workshops adicionados ao congresso por omissão*/
    private static final List<Workshop> WORKSHOPS_POR_OMISSAO = new ArrayList<>();

    public Congresso() {
        super();
        this.workshops = WORKSHOPS_POR_OMISSAO;
    }

    /**
     *
     * @param workshops       Lista de workshops adicionados ao congresso
     */  
    public Congresso(List<Workshop> workshops) {
        super();
        this.workshops = workshops;
    }

    /**
     *
     * @param titulo                    Titulo do evento
     * @param descricao                 Descrição do evento
     * @param dataInicio                Data de inicio do evento
     * @param dataFim                   Data fim do evento
     * @param dataLimiteCandidaturas    Data limite de aceitação de candidaturas
     * @param local                     Local de realização do evento
     * @param organizadores             Organizadores atribuidos ao evento
     * @param funcsApoioEvento          FAE atribuidos ao evento
     * @param candidaturas              Candidaturas realizadas ao evento
     * @param decisoes                  Decisões atribuidas no evento
     * @param workshops                 Workshops registados no evento
     */  
    public Congresso(String titulo,
                     String descricao,
                     Date dataInicio,
                     Date dataFim,
                     Date dataLimiteCandidaturas,
                     Local local,
                     List<Organizador> organizadores,
                     List<FuncApoioEvento> funcsApoioEvento,
                     List<Candidatura> candidaturas,
                     List<Decisao> decisoes,
                     List<Workshop> workshops) {
        super(titulo, descricao, dataInicio, dataFim, dataLimiteCandidaturas, local, organizadores, funcsApoioEvento, candidaturas, decisoes);
        this.workshops = workshops;
    }

    /**
     * @return lista de workshops registados no evento
     */
    public List<Workshop> getWorkshops() {
        return workshops;
    }

    /**
     * Define lista de workshops registados no evento
     *
     * @param workshops registados no evento
     */
    public void setWorkshops(List<Workshop> workshops) {
        this.workshops = workshops;
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Congresso)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Congresso congresso = (Congresso) o;
        return Objects.equals(workshops, congresso.workshops);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), workshops);
    }

    /**
     * @return Congresso alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "Congresso [id=%s, workshops=%s]",
                super.toString(),
                this.workshops);
    }
}
