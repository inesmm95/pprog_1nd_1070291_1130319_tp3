package isep.pprog.ce.dominio;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Objects;

/**
 * A classe {@code Congresso} representa um funcionário de apoio ao evento.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class FuncApoioEvento extends ClasseDominio {
    
    /** Utilizador que tem papel de FAE */
    private Utilizador utilizador;
    
    /** Experiência profissional */
    private ExperienciaProfissional experienciaProfissional;
    
    /** Experiência profissional por omissão*/
    private static final ExperienciaProfissional EXPERIENCIA_PROFISSIONAL_POR_OMISSAO = ExperienciaProfissional.SEM_EXPERIENCIA;

    public FuncApoioEvento() {
        this.experienciaProfissional = EXPERIENCIA_PROFISSIONAL_POR_OMISSAO;
    }
    
    /**
     *
     * @param utilizador                 Utilizador que tem papel de FAE
     * @param experienciaProfissional    Experiência profissional
     */  
    public FuncApoioEvento(Utilizador utilizador, ExperienciaProfissional experienciaProfissional) {
        this.utilizador = new Utilizador(utilizador);
        this.experienciaProfissional = experienciaProfissional;
    }

    /**
     * @return  Utilizador que tem papel de FAE
     */
    public Utilizador getUtilizador() {
        return utilizador;
    }

    /**
     * Define  Utilizador que tem papel de FAE
     *
     * @param utilizador que tem papel de FAE
     */
    public void setUtilizador(Utilizador utilizador) {
        this.utilizador = utilizador;
    }

    /**
     * @return Experiência profissional
     */
    public ExperienciaProfissional getExperienciaProfissional() {
        return experienciaProfissional;
    }

    /**
     * Define Experiência profissional
     *
     * @param experienciaProfissional experiencia profissional
     */
    public void setExperienciaProfissional(ExperienciaProfissional experienciaProfissional) {
        this.experienciaProfissional = experienciaProfissional;
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FuncApoioEvento)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        FuncApoioEvento that = (FuncApoioEvento) o;
        return Objects.equals(utilizador, that.utilizador) &&
                experienciaProfissional == that.experienciaProfissional;
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(utilizador, experienciaProfissional);
    }

    /**
     * @return FuncApoioEvento alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "FuncApoioEvento [id=%s, utilizador=%s, experienciaProfissional=%s]",
                super.toString(),
                this.utilizador != null ? this.utilizador.getNome() : NA,
                this.experienciaProfissional);
    }
}
