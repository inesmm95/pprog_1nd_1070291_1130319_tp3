package isep.pprog.ce.dominio;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Objects;
/**
 * A classe {@code Atribuicao} representa as atribuições das candidaturas pelos FAE.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Atribuicao extends ClasseDominio {
    
    /** Organizador que atribui as candidaturas */
    private Organizador organizador;
    
    /** Evento que a candidatura foi realizada*/
    private Evento evento;

    /** Candidatura atribuida */
    private Candidatura candidatura;

    /** Funcionário de apoio ao evento atribuido à candidatura */    
    private FuncApoioEvento funcApoioEvento;

    public Atribuicao() {
    }

    /**
     *
     * @param organizador       Organizador que atribui a candidatura
     * @param evento            Evento da candidatura
     * @param candidatura       Candidatura atribuida
     * @param funcApoioEvento   Funcionário de apoio ao evento atribuido à candidatura para decidir
     */   
    public Atribuicao(Organizador organizador,
                      Evento evento,
                      Candidatura candidatura,
                      FuncApoioEvento funcApoioEvento) {
        this.organizador = organizador;
        this.evento = evento;
        this.candidatura = candidatura;
        this.funcApoioEvento = funcApoioEvento;
    }

    /**
     * @return o organizador que atribui a candidatura
     */
    public Organizador getOrganizador() {
        return organizador;
    }

    /**
     * Define o organizador que atribui as candidaturas
     *
     * @param organizador que atribui as candidaturas
     */
    public void setOrganizador(Organizador organizador) {
        this.organizador = organizador;
    }

    /**
     * @return o evento associado à candidatura
     */
    public Evento getEvento() {
        return evento;
    }

    /**
     * Define o evento associado à candidatura
     *
     * @param evento associado à candidatura
     */
    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    /**
     * @return a candidatura atribuida ao FAE
     */
    public Candidatura getCandidatura() {
        return candidatura;
    }

    /**
     * Define o candidatura atribuida ao FAE
     *
     * @param candidatura atribuida ao FAE
     */
    public void setCandidatura(Candidatura candidatura) {
        this.candidatura = candidatura;
    }

    /**
     * @return o funcApoioEvento atribuido À candidatura
     */
    public FuncApoioEvento getFuncApoioEvento() {
        return funcApoioEvento;
    }

    /**
     * Define o funcApoioEventoatribuido À candidatura
     *
     * @param funcApoioEvento atribuido À candidatura
     */
    public void setFuncApoioEvento(FuncApoioEvento funcApoioEvento) {
        this.funcApoioEvento = funcApoioEvento;
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Atribuicao)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Atribuicao that = (Atribuicao) o;
        return Objects.equals(organizador, that.organizador) &&
                Objects.equals(evento, that.evento) &&
                Objects.equals(candidatura, that.candidatura) &&
                Objects.equals(funcApoioEvento, that.funcApoioEvento);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(organizador, evento, candidatura, funcApoioEvento);
    }

    /**
     * @return Representação alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "Atribuicao [id=%s organizador=%s, evento=%s, candidatura=%s, funcApoioEvento=%s]",
                super.toString(),
                this.organizador != null ? this.organizador.getUtilizador().getNome() : NA,
                this.evento != null ? this.evento.getDescricao() : NA,
                this.candidatura != null ? this.candidatura.getDescricao() : NA,
                this.funcApoioEvento != null ? this.funcApoioEvento.getUtilizador().getNome() : NA);
    }
}
