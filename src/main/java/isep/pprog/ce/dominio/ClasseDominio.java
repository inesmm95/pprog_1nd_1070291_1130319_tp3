package isep.pprog.ce.dominio;


import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
/**
 * A classe {@code ClasseDominio} é a superclass de qualquer implementação
 * (subclass) de classes específicas de entidades de ClasseDominio.
 * 
 * @author Raul Estrela
 * @author Inês Marques
 */
public abstract class ClasseDominio implements Serializable {
    
    /** identificador único */
    private String id;
    
    /** Identificador único por omissão */
    protected static final String NA = "NA";

    public ClasseDominio() {
        this.id = UUID.randomUUID().toString();
    }

    /**
     * @return o id que identificador
     */
    public String getId() {
        return id;
    }

    /**
     * Define o id que identifica
     *
     * @param id que identifica
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClasseDominio)) {
            return false;
        }
        ClasseDominio that = (ClasseDominio) o;
        return Objects.equals(id, that.id);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    /**
     * @return ClasseDominio alfanumérica do objecto
     */
    @Override
    public String toString() {
        return this.id;
    }
}