package isep.pprog.ce.dominio;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A classe {@code Organizador} representa um organizador do evento.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Organizador extends ClasseDominio {
    
    /** Utilizador que tem papel de organizador */
    private Utilizador utilizador;
     
    /** Lista de eventos atribuidos ao organizador */   
    private List<Evento> eventos;
      
    /** Lista de eventos atribuidos ao organizador por omissão */    
    private static final List<Evento> EVENTOS_POR_OMISSAO = new ArrayList<>();

    public Organizador() {
        this.eventos = EVENTOS_POR_OMISSAO;
    }
    
    /**
     *
     * @param utilizador   Utilizador que tem papel de organizador
     * @param eventos      Lista de eventos atribuidos ao organizador
     */ 
    public Organizador(Utilizador utilizador, List<Evento> eventos) {
        this.utilizador = new Utilizador(utilizador);
        this.eventos = eventos;
    }

    /**
     * @return  Utilizador que tem papel de organizador
     */
    public Utilizador getUtilizador() {
        return utilizador;
    }

    /**
     * Define  Utilizador que tem papel de organizador
     *
     * @param utilizador que tem papel de organizador
     */
    public void setUtilizador(Utilizador utilizador) {
        this.utilizador = utilizador;
    }

    /**
     * @return  Lista de eventos atribuidos ao organizador
     */
    public List<Evento> getEventos() {
        return eventos;
    }

    /**
     * Define  Lista de eventos atribuidos ao organizador
     *
     * @param eventos atribuidos ao organizador
     */
    public void setEventos(List<Evento> eventos) {
        this.eventos = eventos;
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Organizador)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Organizador that = (Organizador) o;
        return Objects.equals(utilizador, that.utilizador) &&
                Objects.equals(eventos, that.eventos);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(utilizador, eventos);
    }

    /**
     * @return Organizador alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "Organizador [id=%s, utilizador=%s, eventos=%s]",
                super.toString(),
                this.utilizador != null ? this.utilizador.getNome() : NA,
                this.eventos);
    }
}
