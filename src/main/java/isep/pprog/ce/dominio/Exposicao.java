package isep.pprog.ce.dominio;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Date;
import java.util.List;

/**
 * A classe {@code Exposicao} representa um evento do tipo Exposição.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Exposicao extends Evento {

    public Exposicao() {
        super();
    }

    /**
     *
     * @param titulo                    Titulo do evento
     * @param descricao                 Descrição do evento
     * @param dataInicio                Data de inicio do evento
     * @param dataFim                   Data fim do evento
     * @param dataLimiteCandidaturas    Data limite de aceitação de candidaturas
     * @param local                     Local de realização do evento
     * @param organizadores             Organizadores atribuidos ao evento
     * @param funcsApoioEvento          FAE atribuidos ao evento
     * @param candidaturas              Candidaturas realizadas ao evento
     * @param decisoes                  Decisões atribuidas no evento
     */  
    public Exposicao(String titulo,
                     String descricao,
                     Date dataInicio,
                     Date dataFim,
                     Date dataLimiteCandidaturas,
                     Local local,
                     List<Organizador> organizadores,
                     List<FuncApoioEvento> funcsApoioEvento,
                     List<Candidatura> candidaturas,
                     List<Decisao> decisoes) {
        super(titulo, descricao, dataInicio, dataFim, dataLimiteCandidaturas, local, organizadores, funcsApoioEvento, candidaturas, decisoes);
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        return this == o || o instanceof Exposicao && super.equals(o);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * @return Exposicao alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format("Exposicao [id=%s]", super.toString());
    }
}
