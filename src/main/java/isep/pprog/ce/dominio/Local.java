package isep.pprog.ce.dominio;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A classe {@code Congresso} representa um local onde ocorrem eventos.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Local extends ClasseDominio {
    
    /** Descrição do local */
    private String descricao;
    
    /** Lista de eventos do local*/
    private List<Evento> eventos;
    
    /** Descrição do local por omissão */
    private static final String DESCRICAO_POR_OMISSAO = "Sem descrição";
    
    /** Lista de eventos do local por omissão */
    private static final List<Evento> EVENTOS_POR_OMISSAO = new ArrayList<>();

    public Local() {
        this.descricao = DESCRICAO_POR_OMISSAO;
        this.eventos = EVENTOS_POR_OMISSAO;
    }

    /**
     *
     * @param descricao   Descrição do local
     * @param eventos     Lista de eventos do local 
     */ 
    public Local(String descricao, List<Evento> eventos) {
        this.descricao = descricao;
        this.eventos = eventos;
    }

    /**
     * @return Descrição do local
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Define Descrição do local
     *
     * @param descricao do local
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return Lista de eventos do local 
     */
    public List<Evento> getEventos() {
        return eventos;
    }

    /**
     * Define Lista de eventos do local 
     *
     * @param eventos registados no local
     */
    public void setEventos(List<Evento> eventos) {
        this.eventos = eventos;
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Local)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Local local = (Local) o;
        return Objects.equals(descricao, local.descricao) &&
                Objects.equals(eventos, local.eventos);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(descricao, eventos);
    }

    /**
     * @return Local alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "Local [id=%s, descricao=%s, eventos=%s]",
                super.toString(),
                this.descricao,
                this.eventos);
    }
}
