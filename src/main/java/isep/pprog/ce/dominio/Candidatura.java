package isep.pprog.ce.dominio;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Objects;

/**
 * A classe {@code Candidatura} representa as candidaturas aos eventos realizadas por representantes
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Candidatura extends ClasseDominio {
    
    /** Descrição da candidatura */
    private String descricao;
    
    /** Representante que realiza a candidatura */
    private Representante representante;
    
    /** Evento a que o representante se candidata */
    private Evento evento;
    
    /** Decisão da candidatura */
    private Decisao decisao;

    public Candidatura() {
    }

    /**
     *
     * @param representante Representante que realiza a candidatura 
     * @param evento        Evento a que o representante se candidata
     * @param decisao       Decisão da candidatura
     */  
    public Candidatura(Representante representante,
                       Evento evento,
                       Decisao decisao) {
        this.representante = representante;
        this.evento = evento;
        this.decisao = decisao;
    }

    /**
     * @return o representante que realiza a candidatura 
     */
    public Representante getRepresentante() {
        return representante;
    }

    /**
     * Define o representante que realiza a candidatura 
     *
     * @param representante que realiza a candidatura  
     */
    public void setRepresentante(Representante representante) {
        this.representante = representante;
    }

    /**
     * @return evento a que o representante se candidata
     */
    public Evento getEvento() {
        return evento;
    }

    /**
     * Define o evento a que o representante se candidata
     *
     * @param evento a que o representante se candidata
     */
    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    /**
     * @return a decisão da candidatura
     */
    public Decisao getDecisao() {
        return decisao;
    }

    /**
     * Define a decisão da candidatura
     *
     * @param decisao da candidatura
     */
    public void setDecisao(Decisao decisao) {
        this.decisao = decisao;
    }

    /**
     * @return a descrição da candidatura
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Define a descrição da candidatura
     *
     * @param descricao da candidatura
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Candidatura)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Candidatura that = (Candidatura) o;
        return Objects.equals(descricao, that.descricao) &&
                Objects.equals(representante, that.representante) &&
                Objects.equals(evento, that.evento) &&
                Objects.equals(decisao, that.decisao);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(descricao, representante, evento, decisao);
    }

    /**
     * @return Candidatura alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "Candidatura [id=%s, descricao=%s, representante=%s, evento=%s, decisao=%s]",
                super.toString(),
                this.descricao,
                this.representante != null ? this.representante.getUtilizador().getNome() : NA,
                this.evento != null ? this.evento.getDescricao() : NA,
                this.decisao);
    }
}
