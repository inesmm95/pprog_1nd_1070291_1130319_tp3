package isep.pprog.ce.dominio;

/**
 * A enum {@code ExperienciaProfissional} representa a experiência profissional.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */

public enum ExperienciaProfissional {

    SENIOR("Senior", 0.4d),
    EXPERIENTE("Experiente", 0.3d),
    JUNIOR("Junior", 0.2d),
    SEM_EXPERIENCIA("Sem experiência", 0.1d);
    
    /** Descrição da experiência */
    private String descricao; 
    
    /** valor de percentagem da experiência por atribuição */
    private double racioTrabalhoPorAtribuicao;

    /**
     *
    * @param descricao                      Descrição da experiência
    * @param racioTrabalhoPorAtribuicao     valor de percentagem da experiência por atribuição
     */
    ExperienciaProfissional(String descricao, double racioTrabalhoPorAtribuicao) {
        this.descricao = descricao;
        this.racioTrabalhoPorAtribuicao = racioTrabalhoPorAtribuicao;
    }

    /**
     * @return Descrição da experiência
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Define Descrição da experiência
     *
     * @param descricao da experiência
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return valor de percentagem da experiência por atribuição
     */
    public double getRacioTrabalhoPorAtribuicao() {
        return racioTrabalhoPorAtribuicao;
    }

    /**
     * Define valor de percentagem da experiência por atribuição
     *
     * @param racioTrabalhoPorAtribuicao Rácio de trabalho por atribuição
     */
    public void setRacioTrabalhoPorAtribuicao(double racioTrabalhoPorAtribuicao) {
        this.racioTrabalhoPorAtribuicao = racioTrabalhoPorAtribuicao;
    }

    /**
     * @return ExperienciaProfissional alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "ExperienciaProfissional [descricao=%s, racioTrabalhoPorAtribuicao=%s]",
                this.descricao,
                this.racioTrabalhoPorAtribuicao);
    }
}
