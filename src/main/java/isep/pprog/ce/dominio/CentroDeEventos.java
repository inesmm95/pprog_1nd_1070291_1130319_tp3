package isep.pprog.ce.dominio;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import isep.pprog.ce.ex.CentroDeEventosException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
/**
 * A classe {@code CentroDeEventos} representa o centro de eventos
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class CentroDeEventos extends ClasseDominio {
    
    /** Nome do centro de eventos */
    private String nome;
    
    /** Morada do centro de Eventos*/
    private String morada;
    
    /** Lista de locais do centro de eventos*/
    private List<Local> locais;
    
    /** Lista de funcionários de apoio ao evento registados no centro de eventos*/
    private List<FuncApoioEvento> funcsApoioEvento;
    
    /** Lista de gestores de eventos registados no centro de eventos */
    private List<GestorDeEventos> gestoresDeEvento;
    
    /** Lista de organizadores registados no centro de eventos */
    private List<Organizador> organizadores;
    
    /** Lista de representante registados no centro de eventos */
    private List<Representante> representantes;
    
    /** Lista de eventos criados no centro de eventos*/
    private List<Evento> eventos;
    
    /** Nome por omissão do centro de eventos*/
    private static final String NOME_CENTRO_EVENTOS_POR_OMISSAO = "sem nome";
    
    /** Morada por omissão do centro de eventos*/
    private static final String MORADA_CENTRO_EVENTOS_POR_OMISSAO = "sem morada";
    
    /** Lista de locais do centro de eventos registados no centro de eventos*/
    private static final List<Local> LOCAIS_POR_OMISSAO = new ArrayList<>();
    
    /** Lista de funcionários de apoio ao evento por omissão registados no centro de eventos*/
    private static final List<FuncApoioEvento> FAES_POR_OMISSAO = new ArrayList<>();
    
   /** Lista de gestores de eventos por omissão registados no centro de eventos */
    private static final List<GestorDeEventos> GESTORES_DE_EVENTO_POR_OMISSAO = new ArrayList<>();
    
    /** Lista de organizadores por omissão registados no centro de eventos */
    private static final List<Organizador> ORGANIZADORES_POR_OMISSAO = new ArrayList<>();
    
    /** Lista de representante por omissão registados no centro de eventos */
    private static final List<Representante> REPRESENTANTES_POR_OMISSAO = new ArrayList<>();
    
     /** Lista de eventos por omissão criados no centro de eventos*/
    private static final List<Evento> EVENTOS_POR_OMISSAO = new ArrayList<>();

    public CentroDeEventos() {
        this.nome = NOME_CENTRO_EVENTOS_POR_OMISSAO;
        this.morada = MORADA_CENTRO_EVENTOS_POR_OMISSAO;
        this.locais = LOCAIS_POR_OMISSAO;
        this.funcsApoioEvento = FAES_POR_OMISSAO;
        this.gestoresDeEvento = GESTORES_DE_EVENTO_POR_OMISSAO;
        this.organizadores = ORGANIZADORES_POR_OMISSAO;
        this.representantes = REPRESENTANTES_POR_OMISSAO;
        this.eventos = EVENTOS_POR_OMISSAO;
    }
 
    /**
     *
     * @param nome              Nome do centro de eventos
     * @param morada            Morada do centro de Eventos
     * @param locais            Lista de locais do centro de eventos
     * @param funcsApoioEvento  Lista de funcionários de apoio ao evento registados no centro de eventos
     * @param gestoresDeEvento  Lista de gestores de eventos registados no centro de eventos 
     * @param organizadores     Lista de organizadores registados no centro de eventos 
     * @param representantes    Lista de representante registados no centro de eventos
     * @param eventos           Lista de eventos criados no centro de eventos
     */
    public CentroDeEventos(String nome,
                           String morada,
                           List<Local> locais,
                           List<FuncApoioEvento> funcsApoioEvento,
                           List<GestorDeEventos> gestoresDeEvento,
                           List<Organizador> organizadores,
                           List<Representante> representantes,
                           List<Evento> eventos) {
        this.nome = nome;
        this.morada = morada;
        this.locais = locais;
        this.funcsApoioEvento = funcsApoioEvento;
        this.gestoresDeEvento = gestoresDeEvento;
        this.organizadores = organizadores;
        this.representantes = representantes;
        this.eventos = eventos;
    }

    /**
     * @return o nome do centro de eventos
     */
    public String getNome() {
        return nome;
    }

    /**
     * Define Nome do centro de eventos
     *
     * @param nome do centro de eventos
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return morada do centro de Eventos
     */
    public String getMorada() {
        return morada;
    }

    /**
     * Define o morada do centro de Eventos
     *
     * @param morada do centro de Eventos
     */
    public void setMorada(String morada) {
        this.morada = morada;
    }

    /**
     * @return lista de locais do centro de eventos
     */
    public List<Local> getLocais() {
        return locais;
    }

    /**
     * Define lista de locais do centro de eventos
     *
     * @param locais do centro de eventos
     */
    public void setLocais(List<Local> locais) {
        this.locais = locais;
    }

    /**
     * @return lista de funcionários de apoio ao evento registados no centro de eventos
     */
    public List<FuncApoioEvento> getFuncsApoioEvento() {
        return funcsApoioEvento;
    }

    /**
     * Define lista de funcionários de apoio ao evento registados no centro de eventos
     *
     * @param funcsApoioEvento lista de funcionários de apoio ao evento registados no centro de eventos
     */
    public void setFuncsApoioEvento(List<FuncApoioEvento> funcsApoioEvento) {
        this.funcsApoioEvento = funcsApoioEvento;
    }

    /**
     * @return lista de gestores de eventos registados no centro de eventos 
     */
    public List<GestorDeEventos> getGestoresDeEvento() {
        return gestoresDeEvento;
    }

    /**
     * Define lista de gestores de eventos registados no centro de eventos 
     *
     * @param gestoresDeEvento lista de gestores de eventos registados no centro de eventos 
     */
    public void setGestoresDeEvento(List<GestorDeEventos> gestoresDeEvento) {
        this.gestoresDeEvento = gestoresDeEvento;
    }

    /**
     * @return lista de organizadores registados no centro de eventos 
     */
    public List<Organizador> getOrganizadores() {
        return organizadores;
    }

    /**
     * Define lista de organizadores registados no centro de eventos 
     *
     * @param organizadores lista de organizadores registados no centro de eventos 
     */
    public void setOrganizadores(List<Organizador> organizadores) {
        this.organizadores = organizadores;
    }

    /**
     * @return lista de representante registados no centro de eventos
     */
    public List<Representante> getRepresentantes() {
        return representantes;
    }

    /**
     * Define lista de representante registados no centro de eventos
     *
     * @param representantes lista de representante registados no centro de eventos
     */
    public void setRepresentantes(List<Representante> representantes) {
        this.representantes = representantes;
    }

    /**
     * @return lista de eventos criados no centro de eventos
     */
    public List<Evento> getEventos() {
        return eventos;
    }

    /**
     * Define lista de eventos criados no centro de eventos
     *
     * @param eventos lista de eventos criados no centro de eventos
     */
    public void setEventos(List<Evento> eventos) {
        this.eventos = eventos;
    }
    
    /**
     * @param nomeDeUtilizador Nome de utilizador
     *
     * @return utilizador pelo nome de utilizador
     */
    public Utilizador obterUtilizadorByNomeDeUtilizador(String nomeDeUtilizador) {
        Utilizador utilizador = null;

        for(FuncApoioEvento f: funcsApoioEvento) {
            if (f.getUtilizador().getUsername().equals(nomeDeUtilizador)) {
                utilizador = f.getUtilizador();
            }
        }

        for(GestorDeEventos g: gestoresDeEvento) {
            if (g.getUtilizador().getUsername().equals(nomeDeUtilizador)) {
                utilizador = g.getUtilizador();
            }
        }

        for(Representante r: representantes) {
            if (r.getUtilizador().getUsername().equals(nomeDeUtilizador)) {
                utilizador = r.getUtilizador();
            }
        }

        for(Organizador o: organizadores) {
            if (o.getUtilizador().getUsername().equals(nomeDeUtilizador)) {
                utilizador = o.getUtilizador();
            }
        }

        for(FuncApoioEvento f: funcsApoioEvento) {
            if (f.getUtilizador().getUsername().equals(nomeDeUtilizador)) {
                utilizador = f.getUtilizador();
            }
        }
        return utilizador;
    }
    
    /**
     * @param utilizador Utilizador
     *
     * @return {@code true} se o utilizador tiver o papel de organizador, {@code false} caso contrário
     */
    public boolean isOrganizador(Utilizador utilizador) {
        for (Organizador o: organizadores) {
            if (o.getUtilizador().equals(utilizador)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * @param utilizador Utilizador
     *
     * @return {@code true} se o utilizador tiver o papel de funcionário de apoio ao evento, {@code false} caso contrário
     */
    public boolean isFuncApoioEvento(Utilizador utilizador) {
        for (FuncApoioEvento f: funcsApoioEvento) {
            if (f.getUtilizador().equals(utilizador)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * @param utilizador Utilizador
     *
     * @return {@code true} se o utilizador tiver o papel de representante, {@code false} caso contrário
     */
    public boolean isRepresentante(Utilizador utilizador) {
        for (Representante r: representantes) {
            if (r.getUtilizador().equals(utilizador)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * @param utilizador Utilizador
     *
     * @return o organizador 
     */
    public Organizador obterOrganizadorByUtilizador(Utilizador utilizador) {
        for (Organizador o: organizadores) {
            if (o.getUtilizador().equals(utilizador)) {
                return o;
            }
        }
        throw new CentroDeEventosException("O Organizador não existe no sistema");
    }
    
    /**
     * @param utilizador Utilizador
     *
     * @return o funcionário de apoio ao evento 
     */
    public FuncApoioEvento obterFuncApoioEventoByUtilizador(Utilizador utilizador) {
        for (FuncApoioEvento f: funcsApoioEvento) {
            if (f.getUtilizador().equals(utilizador)) {
                return f;
            }
        }
        throw new CentroDeEventosException("O FAE não existe no sistema");
    }
    
    /**
     * @param utilizador Utilizador
     *
     * @return o representante 
     */
    public Representante obterRepresentanteByUtilizador(Utilizador utilizador) {
        for (Representante r: representantes) {
            if (r.getUtilizador().equals(utilizador)) {
                return r;
            }
        }
        throw new CentroDeEventosException("O Representante não existe no sistema");
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CentroDeEventos)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        CentroDeEventos that = (CentroDeEventos) o;
        return Objects.equals(nome, that.nome) &&
                Objects.equals(morada, that.morada) &&
                Objects.equals(locais, that.locais) &&
                Objects.equals(funcsApoioEvento, that.funcsApoioEvento) &&
                Objects.equals(gestoresDeEvento, that.gestoresDeEvento) &&
                Objects.equals(organizadores, that.organizadores) &&
                Objects.equals(representantes, that.representantes) &&
                Objects.equals(eventos, that.eventos);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(nome, morada, locais, funcsApoioEvento, gestoresDeEvento, organizadores, representantes, eventos);
    }

    /**
     * @return CentroDeEventos alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "CentroDeEventos [id=%s, nome=%s, morada=%s, locais=%s, funcsApoioEvento=%s, gestoresDeEvento=%s, organizadores=%s, " +
                        "representantes=%s, eventos=%s]",
                super.toString(),
                this.nome,
                this.morada,
                this.locais,
                this.funcsApoioEvento,
                this.gestoresDeEvento,
                this.organizadores,
                this.representantes,
                this.eventos);
    }
}
