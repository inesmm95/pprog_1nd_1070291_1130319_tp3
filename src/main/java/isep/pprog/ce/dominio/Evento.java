package isep.pprog.ce.dominio;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
/**
 * A classe {@code Evento} representa um evento,
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
@JsonDeserialize(as = Congresso.class)
public abstract class Evento extends ClasseDominio {
    
    /** Titulo do evento */
    private String titulo;
    
    /** Descrição do evento */
    private String descricao;
    
    /** Data inicio do evento */
    private Date dataInicio;
    
    /** Data fim do evento */
    private Date dataFim;
    
    /** Data limite de candidatura ao evento */
    private Date dataLimiteCandidaturas;
    
    /** Local do evento */
    private Local local;
    
    /** Lista de organizadores atribuidos ao evento */
    private List<Organizador> organizadores;
    
    /** Lista de FAE atribuidos ao evento*/
    private List<FuncApoioEvento> funcsApoioEvento;
    
    /** Lista de candidaturas realizadas ao evento*/
    private List<Candidatura> candidaturas;
    
    /** Lista de atribuições realizadas no evento */
    private List<Atribuicao> atribuicoes;
     
    /** Lista de decisões atribuidas no evento*/   
    private List<Decisao> decisoes; 
    
    /**  Titulo do evento por omissão */
    private static final String TITULO_POR_OMISSAO = "sem título";
    
    /** Descrição do evento por omissão */
    private static final String DESCRICAO_POR_OMISSAO = "sem descrição";
    
    /** Data inicio do evento por omissão */
    private static final Date DATA_INICIO_POR_OMISSAO = new Date();
    
    /** Data fim do evento por omissão */
    private static final Date DATA_FIM_POR_OMISSAO = new Date();
    
    /**  Data limite de candidatura ao evento por omissão */
    private static final Date DATA_LIMITE_CANDIDATURAS_POR_OMISSAO = new Date();
    
    /** Lista de organizadores atribuidos ao evento por omissão */
    private static final List<Organizador> ORGANIZADORES_POR_OMISSAO = new ArrayList<>();
    
    /** Lista de FAE atribuidos ao evento por omissão */
    private static final List<FuncApoioEvento> FAES_POR_OMISSAO = new ArrayList<>();
    
    /** Lista de candidaturas realizadas ao evento por omissão */
    private static final List<Candidatura> CANDIDATURAS_POR_OMISSAO = new ArrayList<>();
    
    /** Lista de atribuições realizadas no evento por omissão */
    private static final List<Atribuicao> ATRIBUICOES_POR_OMISSAO = new ArrayList<>();
    
    /** Lista de decisões atribuidas no evento por omissão */    
    private static final List<Decisao> DECISOES_POR_OMISSAO = new ArrayList<>();

    public Evento() {
        this.titulo = TITULO_POR_OMISSAO;
        this.descricao = DESCRICAO_POR_OMISSAO;
        this.dataInicio = DATA_INICIO_POR_OMISSAO;
        this.dataFim = DATA_FIM_POR_OMISSAO;
        this.dataLimiteCandidaturas = DATA_LIMITE_CANDIDATURAS_POR_OMISSAO;
        this.organizadores = ORGANIZADORES_POR_OMISSAO;
        this.funcsApoioEvento = FAES_POR_OMISSAO;
        this.candidaturas = CANDIDATURAS_POR_OMISSAO;
        this.atribuicoes = ATRIBUICOES_POR_OMISSAO;
        this.decisoes = DECISOES_POR_OMISSAO;
    }
    /**
     *
     * @param titulo                    Titulo do evento
     * @param descricao                 Descrição do evento
     * @param dataInicio                Data de inicio do evento
     * @param dataFim                   Data fim do evento
     * @param dataLimiteCandidaturas    Data limite de aceitação de candidaturas
     * @param local                     Local de realização do evento
     * @param organizadores             Organizadores atribuidos ao evento
     * @param funcsApoioEvento          FAE atribuidos ao evento
     * @param candidaturas              Candidaturas realizadas ao evento
     * @param decisoes                  Decisões atribuidas no evento
     */  
    public Evento(String titulo,
                  String descricao,
                  Date dataInicio,
                  Date dataFim,
                  Date dataLimiteCandidaturas,
                  Local local,
                  List<Organizador> organizadores,
                  List<FuncApoioEvento> funcsApoioEvento,
                  List<Candidatura> candidaturas,
                  List<Decisao> decisoes) {
        this.titulo = titulo;
        this.descricao = descricao;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.dataLimiteCandidaturas = dataLimiteCandidaturas;
        this.local = local;
        this.organizadores = organizadores;
        this.funcsApoioEvento = funcsApoioEvento;
        this.candidaturas = candidaturas;
        this.decisoes = decisoes;
    }

    /**
     * @return Titulo do evento
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * Define Titulo do evento
     *
     * @param titulo do evento
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return Descrição do evento
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Define Descrição do evento
     *
     * @param descricao do evento
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return Data inicio do evento
     */
    public Date getDataInicio() {
        return dataInicio;
    }

    /**
     * Define Data inicio do evento
     *
     * @param dataInicio do evento
     */
    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    /**
     * @return Data fim do evento
     */
    public Date getDataFim() {
        return dataFim;
    }

    /**
     * Define Data fim do evento
     *
     * @param dataFim do evento
     */
    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    /**
     * @return Data limite de candidatura ao evento
     */
    public Date getDataLimiteCandidaturas() {
        return dataLimiteCandidaturas;
    }

    /**
     * Define Data limite de candidatura ao evento
     *
     * @param dataLimiteCandidaturas ao evento
     */
    public void setDataLimiteCandidaturas(Date dataLimiteCandidaturas) {
        this.dataLimiteCandidaturas = dataLimiteCandidaturas;
    }

    /**
     * @return Local do evento
     */
    public Local getLocal() {
        return local;
    }

    /**
     * Define Local do evento
     *
     * @param local do evento
     */
    public void setLocal(Local local) {
        this.local = local;
    }

    /**
     * @return Lista de organizadores atribuidos ao evento 
     */
    public List<Organizador> getOrganizadores() {
        return organizadores;
    }

    /**
     * Define Lista de organizadores atribuidos ao evento 
     *
     * @param organizadores atribuidos ao evento 
     */
    public void setOrganizadores(List<Organizador> organizadores) {
        this.organizadores = organizadores;
    }

    /**
     * @return Lista de FAE atribuidos ao evento
     */
    public List<FuncApoioEvento> getFuncsApoioEvento() {
        return funcsApoioEvento;
    }

    /**
     * Define Lista de FAE atribuidos ao evento
     *
     * @param funcsApoioEvento atribuidos ao evento
     */
    public void setFuncsApoioEvento(List<FuncApoioEvento> funcsApoioEvento) {
        this.funcsApoioEvento = funcsApoioEvento;
    }

    /**
     * @return Lista de candidaturas realizadas ao evento
     */
    public List<Candidatura> getCandidaturas() {
        return candidaturas;
    }

    /**
     * Define Lista de candidaturas realizadas ao evento
     *
     * @param candidaturas realizadas ao evento
     */
    public void setCandidaturas(List<Candidatura> candidaturas) {
        this.candidaturas = candidaturas;
    }

    /**
     * @return Lista de atribuições realizadas no evento 
     */
    public List<Atribuicao> getAtribuicoes() {
        return atribuicoes;
    }

    /**
     * Define Lista de atribuições realizadas no evento 
     *
     * @param atribuicoes realizadas no evento 
     */
    public void setAtribuicoes(List<Atribuicao> atribuicoes) {
        this.atribuicoes = atribuicoes;
    }

    /**
     * @return Lista de decisões atribuidas no evento
     */
    public List<Decisao> getDecisoes() {
        return decisoes;
    }

    /**
     * Define Lista de decisões atribuidas no evento
     *
     * @param decisoes atribuidas no evento
     */
    public void setDecisoes(List<Decisao> decisoes) {
        this.decisoes = decisoes;
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Evento evento = (Evento) o;
        return Objects.equals(titulo, evento.titulo) &&
                Objects.equals(descricao, evento.descricao) &&
                Objects.equals(dataInicio, evento.dataInicio) &&
                Objects.equals(dataFim, evento.dataFim) &&
                Objects.equals(dataLimiteCandidaturas, evento.dataLimiteCandidaturas) &&
                Objects.equals(local, evento.local) &&
                Objects.equals(organizadores, evento.organizadores) &&
                Objects.equals(funcsApoioEvento, evento.funcsApoioEvento) &&
                Objects.equals(candidaturas, evento.candidaturas) &&
                Objects.equals(atribuicoes, evento.atribuicoes);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), titulo, descricao, dataInicio, dataFim, dataLimiteCandidaturas, local,
                organizadores, funcsApoioEvento, candidaturas, atribuicoes, decisoes);
    }

    /**
     * @return Evento alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "Evento [id=%s, titulo=%s, descricao=%s, dataInicio=%s, dataFim=%s, dataLimiteCandidaturas=%s, local=%s, " +
                        "organizadores=%s, funcsApoioEvento=%s, candidaturas=%s, atribuicoes=%s, decisoes=%s]",
                super.toString(),
                this.titulo,
                this.descricao,
                this.dataInicio,
                this.dataFim,
                this.dataLimiteCandidaturas,
                this.local,
                this.organizadores,
                this.funcsApoioEvento,
                this.candidaturas,
                this.atribuicoes,
                this.decisoes);
    }
}
