package isep.pprog.ce.dominio;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Objects;

/**
 * A classe {@code Representante} representa um representante.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Representante extends ClasseDominio {
    
    /** Utilizador que tem papel de representante */
    private Utilizador utilizador;
     
    /** Contacto do representante */   
    private String contacto;
     
    /** Contacto do representante por omissão */   
    private static final String CONTACTO_POR_OMISSAO = "sem contacto";

    public Representante() {
        this.contacto = CONTACTO_POR_OMISSAO;
    }
    
    /**
     *
     * @param utilizador Utilizador que tem papel de FAE
     * @param contacto   Contacto do representante
     */  
    public Representante(Utilizador utilizador, String contacto) {
        this.utilizador = new Utilizador(utilizador);
        this.contacto = contacto;
    }

    /**
     * @return  Utilizador que tem papel de representante
     */
    public Utilizador getUtilizador() {
        return utilizador;
    }

    /**
     * Define  Utilizador que tem papel de representante
     *
     * @param utilizador que tem papel de representante
     */
    public void setUtilizador(Utilizador utilizador) {
        this.utilizador = utilizador;
    }

    /**
     * @return  Contacto do representante
     */
    public String getContacto() {
        return contacto;
    }

    /**
     * Define  Contacto do representante
     *
     * @param contacto do representante
     */
    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Representante)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Representante that = (Representante) o;
        return Objects.equals(utilizador, that.utilizador) &&
                Objects.equals(contacto, that.contacto);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(utilizador, contacto);
    }

    /**
     * @return Representante alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "Representante [id=%s, utilizador=%s, contacto=%s]",
                super.toString(),
                this.utilizador != null ? this.utilizador.getNome() : NA,
                this.contacto);
    }
}
