package isep.pprog.ce.dominio;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Objects;

/**
 * A classe {@code Workshop} representa um workshop de um Congresso.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Workshop extends ClasseDominio {
    
    /** Tema do Workshop */
    private String tema;
    
    /** Perito associado ao workshop */
    private String perito;
    
    /** Tema do Workshop por omissão */
    private final static String TEMA_POR_OMISSAO = "sem tema";
    
    /** Lista de workshops adicionados ao congresso */
    private final static String PERITO_POR_OMISSAO = "sem perito";

    public Workshop() {
        this.perito = PERITO_POR_OMISSAO;
        this.tema = TEMA_POR_OMISSAO;
    }

    /**
     *
     * @param tema    Tema do Workshop 
     * @param perito  Perito associado ao workshop 
     */  
    public Workshop(String tema, String perito) {
        this.tema = tema;
        this.perito = perito;
    }

    /**
     * @return Tema do Workshop 
     */
    public String getTema() {
        return tema;
    }

    /**
     * Define Tema do Workshop 
     *
     * @param tema do Workshop 
     */
    public void setTema(String tema) {
        this.tema = tema;
    }

    /**
     * @return  Perito associado ao workshop 
     */
    public String getPerito() {
        return perito;
    }

    /**
     * Define  Perito associado ao workshop 
     *
     * @param  perito associado ao workshop 
     */
    public void setPerito(String perito) {
        this.perito = perito;
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Workshop)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Workshop workshop = (Workshop) o;
        return Objects.equals(tema, workshop.tema) &&
                Objects.equals(perito, workshop.perito);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(tema, perito);
    }

    /**
     * @return Workshop alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "Workshop [id=%s, tema=%s, perito=%s]",
                super.toString(),
                this.tema,
                this.perito);
    }

}
