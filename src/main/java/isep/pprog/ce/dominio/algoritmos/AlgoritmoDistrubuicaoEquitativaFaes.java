package isep.pprog.ce.dominio.algoritmos;


import isep.pprog.ce.dominio.*;
import isep.pprog.ce.ex.AlgorimoAtribuicaoException;

import java.util.ArrayList;
import java.util.List;

/**
 * A classe {@code AlgoritmoDistrubuicaoEquitativaFaes} representa um algoritmo especifico de distribuição
 * de algoritmos por FAEs. Neste algoritmo as candidaturas são distribuídas em igual número pelos FAEs.
 *
 * Este algoritmo é apenas aplicável há candidaturas e FAEs associados ao evento e quando a divisão do
 * total de candidaturas pelo número de FAEs resulta num numero inteiro. Esta pré-condição é verificada
 * na instanciação da classe.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public class AlgoritmoDistrubuicaoEquitativaFaes extends AbstractAlgoritmoAtribuicao implements AlgoritmoAtribuicaoCandidaturas {

    /** Nome do algoritmo */
    public static final String NOME_DO_ALGORITMO = "Distribuição equitativa pelos FAE";

    /**
     * @param evento      Evento
     * @param organizador Organizador
     * @throws AlgorimoAtribuicaoException Caso as pré-condições para a execução do algoritmo não se verifiquem
     */
    public AlgoritmoDistrubuicaoEquitativaFaes(Evento evento, Organizador organizador) throws AlgorimoAtribuicaoException {
        super(NOME_DO_ALGORITMO, evento, organizador);
    }

    /**
     * Executa as atribuições
     *
     * @return List Atribuicao As atribuições resultantes da execução do algoritmo
     */
    @Override
    public List<Atribuicao> atribui() {
        Evento evento = super.getEvento();
        int numeroDeCandidaturasPorFae = evento.getCandidaturas().size() / evento.getFuncsApoioEvento().size();
        List<Candidatura> candidaturasAoEvento = evento.getCandidaturas();
        List<FuncApoioEvento> funcsApoioEvento = evento.getFuncsApoioEvento();

        List<Atribuicao> atribuicoes = new ArrayList<>();
        int nrCandidaturasAtribuidas = 0;
        for (FuncApoioEvento funcApoioEvento : funcsApoioEvento) {
            for (int j = 0; j < numeroDeCandidaturasPorFae; j++) {
                Atribuicao atribuicao = new Atribuicao();
                atribuicao.setOrganizador(super.getOrganizador());
                atribuicao.setFuncApoioEvento(funcApoioEvento);
                atribuicao.setEvento(evento);
                atribuicao.setCandidatura(candidaturasAoEvento.get(nrCandidaturasAtribuidas));
                atribuicoes.add(atribuicao);
                nrCandidaturasAtribuidas++;
            }
        }
        return atribuicoes;
    }

    /**
     * Valida os dados de execução do algoritmo
     *
     * @throws AlgorimoAtribuicaoException Caso as pré-condições para a execução do algoritmo não se verifiquem
     */
    @Override
    public void validaDadosExecucaoAlgoritmo() throws AlgorimoAtribuicaoException {
        Evento evento = super.getEvento();
        if (evento.getFuncsApoioEvento().isEmpty() ||
                evento.getCandidaturas().isEmpty() ||
                evento.getCandidaturas().size() % evento.getFuncsApoioEvento().size() != 0) {
            throw new AlgorimoAtribuicaoException("Não é possível distribuir equitivamente as candidaturas pelos FAES");
        }
    }

    /**
     * @return Representação alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format("Algoritmo %s %s", NOME_DO_ALGORITMO, super.toString());
    }
}
