package isep.pprog.ce.dominio.algoritmos;


import isep.pprog.ce.dominio.*;
import isep.pprog.ce.ex.AlgorimoAtribuicaoException;

import java.util.ArrayList;
import java.util.List;

/**
 * A classe {@code AlgoritmoFaesPorCandidatura} representa um algoritmo especifico de distribuição
 * de algoritmos por FAEs. Neste algoritmo as candidaturas são distribuídas por um número de FAEs
 * definido pelo utilizador
 *
 * Este algoritmo é apenas aplicável quando há candidaturas e FAEs associados ao evento e a multiplicação
 * do número de FAEs pelo número de FAEs definido pelo utilizador é igual ao número total de candidaturas.
 * Esta pré-condição é verificada na instanciação da classe.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public class AlgoritmoFaesPorCandidatura extends AbstractAlgoritmoAtribuicao implements AlgoritmoAtribuicaoCandidaturas {

    /** Nome do algoritmo */
    public final static String NOME_DO_ALGORITMO = "Número de FAE pretendidos por candidatura";

    /** Número de FAEs por candidatura por omissão */
    private static final int FAES_POR_CANDIDATURA_POR_OMISSAO = 2;

    /**
     * @param faesPorCandidatura            faesPorCandidatura Numero de FAEs por candidatura
     * @param evento                        Evento
     * @param organizador                   Organizador
     * @throws AlgorimoAtribuicaoException  Caso as pré-condições para a execução do algoritmo não se verifiquem
     */
    public AlgoritmoFaesPorCandidatura(int faesPorCandidatura,
                                       Evento evento, Organizador
                                       organizador) throws AlgorimoAtribuicaoException {
        super(NOME_DO_ALGORITMO, faesPorCandidatura, evento, organizador);
    }

    /**
     * Executa as atribuições
     *
     * @return List Atribuicao As atribuições resultantes da execução do algoritmo
     */
    @Override
    public List<Atribuicao> atribui() {
        Evento evento = super.getEvento();
        List<Candidatura> candidaturasAoEvento = evento.getCandidaturas();
        List<FuncApoioEvento> funcsApoioEvento = evento.getFuncsApoioEvento();

        List<Atribuicao> atribuicoes = new ArrayList<>();
        int nrCandidaturasAtribuidas = 0;
        for (FuncApoioEvento funcApoioEvento : funcsApoioEvento) {
            for (int j = 0; j < super.getFaesPorCandidatura(); j++) {
                Atribuicao atribuicao = new Atribuicao();
                atribuicao.setOrganizador(super.getOrganizador());
                atribuicao.setFuncApoioEvento(funcApoioEvento);
                atribuicao.setEvento(evento);
                atribuicao.setCandidatura(candidaturasAoEvento.get(nrCandidaturasAtribuidas));
                atribuicoes.add(atribuicao);
                nrCandidaturasAtribuidas++;
            }
        }
        return atribuicoes;
    }

    /**
     * Valida os dados de execução do algoritmo
     *
     * @throws AlgorimoAtribuicaoException Caso as pré-condições para a execução do algoritmo não se verifiquem
     */
    @Override
    public void validaDadosExecucaoAlgoritmo() throws AlgorimoAtribuicaoException {
        if (super.getFaesPorCandidatura() == FAES_POR_CANDIDATURA_POR_OMISSAO) {
            throw new AlgorimoAtribuicaoException("Número inválido de FAEs por candidatura");
        } else if (!validaNumeroDeCandidaturasEFaes()) {
            throw new AlgorimoAtribuicaoException("Número inválido de candidaturas e FAEs para o número de FAEs " +
                    "por candidatura pretendido");
        }

    }

    private boolean validaNumeroDeCandidaturasEFaes() {
        Evento evento = super.getEvento();
        return !evento.getFuncsApoioEvento().isEmpty() &&
               !evento.getCandidaturas().isEmpty() &&
               !(evento.getFuncsApoioEvento().size() * super.getFaesPorCandidatura() != evento.getCandidaturas().size());
    }

    /**
     * @return Representação alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format("Algoritmo %s %s", NOME_DO_ALGORITMO, super.toString());
    }
}
