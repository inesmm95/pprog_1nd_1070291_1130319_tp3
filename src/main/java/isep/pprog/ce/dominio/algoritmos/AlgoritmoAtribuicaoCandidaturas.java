package isep.pprog.ce.dominio.algoritmos;


import isep.pprog.ce.dominio.Atribuicao;

import java.util.List;

/**
 * Representa um algoritmo de atribuição de candidaturas que
 * retorna uma lista de atribuições
 */
public interface AlgoritmoAtribuicaoCandidaturas {

    List<Atribuicao> atribui();

}
