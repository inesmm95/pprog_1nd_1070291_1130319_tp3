package isep.pprog.ce.dominio.algoritmos;


import isep.pprog.ce.dominio.*;
import isep.pprog.ce.ex.AlgorimoAtribuicaoException;

import java.util.*;

/**
 * A classe {@code AlgoritmoExperienciaProfissionalFaes} representa um algoritmo especifico de distribuição
 * de algoritmos por FAEs. Neste algoritmo as candidaturas são de acordo com a experiência profissional dos
 * FAEs atribuidos ao evento. De acordo com {@link ExperienciaProfissional} cada grupo de FAEs correspondente
 * a um determinado nível de experiência ficará encarregado de um percentual (rácio) de candidaturas.
 *
 * Este algoritmo é apenas aplicável quando há candidaturas e FAEs associados ao evento. Esta pré-condição
 * é verificada na instanciação da classe.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public class AlgoritmoExperienciaProfissionalFaes extends AbstractAlgoritmoAtribuicao implements AlgoritmoAtribuicaoCandidaturas {

    /** Nome do algoritmo */
    public static final String NOME_DO_ALGORITMO = "Experiência Profissional";

    /** Lista de candidaturas a atribuir apenas para suporte ao algoritmo */
    private final List<Candidatura> candidaturasAtribuir;

    /**
     * @param evento                       Evento
     * @param organizador                  Organizador
     * @throws AlgorimoAtribuicaoException Caso as pré-condições para a execução do algoritmo não se verifiquem
     */
    public AlgoritmoExperienciaProfissionalFaes(Evento evento, Organizador organizador) throws AlgorimoAtribuicaoException {
        super(NOME_DO_ALGORITMO, evento, organizador);
        this.candidaturasAtribuir = new ArrayList<>(super.getEvento().getCandidaturas());
    }

    /**
     * Executa as atribuições
     *
     * @return List Atribuicao  As atribuições resultantes da execução do algoritmo
     */
    @Override
    public List<Atribuicao> atribui() {

        Map<ExperienciaProfissional, List<FuncApoioEvento>> faesPorExperiencia = initMapaFaesPorExperiencia();
        Map<ExperienciaProfissional, Integer> atribuicoesPorExperiencia = initMapaAtribuicoesPorExperiencia();
        Map<ExperienciaProfissional, Integer> numeroMaxCandidaturasPorExperiencia = initMapaMaxCandidaturasPorExperiencia();

        List<Atribuicao> atribuicoes = new ArrayList<>();
        for (ExperienciaProfissional e: ExperienciaProfissional.values()) {
            List<FuncApoioEvento> funcionariosApoioEvento = faesPorExperiencia.get(e);
            if (funcionariosApoioEvento != null && !funcionariosApoioEvento.isEmpty() &&
                    atribuicoesPorExperiencia.get(e) <= numeroMaxCandidaturasPorExperiencia.get(e)) {
                while (!candidaturasAtribuir.isEmpty() &&
                        !atribuicoesPorExperiencia.get(e).equals(numeroMaxCandidaturasPorExperiencia.get(e))) {
                    for (FuncApoioEvento f: funcionariosApoioEvento) {
                        Candidatura candidatura = candidaturasAtribuir.iterator().next();
                        Atribuicao atribuicao = new Atribuicao();
                        atribuicao.setOrganizador(super.getOrganizador());
                        atribuicao.setFuncApoioEvento(f);
                        atribuicao.setEvento(super.getEvento());
                        atribuicao.setCandidatura(candidatura);
                        atribuicoes.add(atribuicao);
                        candidaturasAtribuir.remove(candidatura);
                        int c = atribuicoesPorExperiencia.get(e);
                        atribuicoesPorExperiencia.put(e, c + 1);
                    }
                }
            }
        }
        return atribuicoes;

    }

    private Map<ExperienciaProfissional, Integer> initMapaAtribuicoesPorExperiencia() {
        Map<ExperienciaProfissional, Integer> atribuicoesPorExperiencia = new HashMap<>();
        for (ExperienciaProfissional experienciaProfissional : ExperienciaProfissional.values()) {
            atribuicoesPorExperiencia.put(experienciaProfissional, 0);
        }
        return atribuicoesPorExperiencia;
    }

    private Map<ExperienciaProfissional, Integer> initMapaMaxCandidaturasPorExperiencia() {
        Map<ExperienciaProfissional, Integer> numeroMaxCandidaturasPorExperiencia = new HashMap<>();
        for (ExperienciaProfissional experienciaProfissional : ExperienciaProfissional.values()) {
            numeroMaxCandidaturasPorExperiencia.put(experienciaProfissional,
                    (int) Math.ceil(candidaturasAtribuir.size() * experienciaProfissional.getRacioTrabalhoPorAtribuicao()));
        }
        return numeroMaxCandidaturasPorExperiencia;
    }

    private Map<ExperienciaProfissional, List<FuncApoioEvento>> initMapaFaesPorExperiencia() {
        Map<ExperienciaProfissional, List<FuncApoioEvento>> faesPorExperiencia = new HashMap<>();
        for (FuncApoioEvento fae: super.getEvento().getFuncsApoioEvento()) {
            ExperienciaProfissional ep = fae.getExperienciaProfissional();
            if (faesPorExperiencia.containsKey(ep)) {
                List<FuncApoioEvento> faes = faesPorExperiencia.get(ep);
                faes.add(fae);
                faesPorExperiencia.put(ep, faes);
            } else {
                List<FuncApoioEvento> funcsApoioEvento = new ArrayList<>();
                funcsApoioEvento.add(fae);
                faesPorExperiencia.put(ep, funcsApoioEvento);
            }
        }
        return faesPorExperiencia;
    }

    /**
     * Valida os dados de execução do algoritmo
     *
     * @throws AlgorimoAtribuicaoException Caso as pré-condições para a execução do algoritmo não se verifiquem
     */
    @Override
    public void validaDadosExecucaoAlgoritmo() throws AlgorimoAtribuicaoException {
        Evento evento = super.getEvento();
        if (evento.getFuncsApoioEvento().isEmpty() || evento.getCandidaturas().isEmpty()) {
            throw new AlgorimoAtribuicaoException("Número inválido de candidaturas/FAEs");
        }
    }

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AlgoritmoExperienciaProfissionalFaes)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AlgoritmoExperienciaProfissionalFaes that = (AlgoritmoExperienciaProfissionalFaes) o;
        return Objects.equals(candidaturasAtribuir, that.candidaturasAtribuir);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), candidaturasAtribuir);
    }

    /**
     * @return Representação alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
            "Algoritmo %s %s [candidaturasAtribuir=%s]", NOME_DO_ALGORITMO, super.toString(), this.candidaturasAtribuir);
    }
}

