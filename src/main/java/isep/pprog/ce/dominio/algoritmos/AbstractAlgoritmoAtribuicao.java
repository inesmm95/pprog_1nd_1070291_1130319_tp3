package isep.pprog.ce.dominio.algoritmos;


import isep.pprog.ce.dominio.Evento;
import isep.pprog.ce.dominio.Organizador;
import isep.pprog.ce.ex.AlgorimoAtribuicaoException;

import java.util.Objects;

/**
 * A classe {@code AbstractAlgoritmoAtribuicao} é a superclass de qualquer implementação
 * (subclass) de classes específicas de algoritmos de atribuição.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public abstract class AbstractAlgoritmoAtribuicao {

    /** Nome do algoritmos */
    private final String nomeAlgoritmo;

    /** Número de FAEs por candidatura */
    private final int faesPorCandidatura;

    /** Evento no qual a candidatura foi submetida */
    private final Evento evento;

    /** Organizador do evento */
    private final Organizador organizador;

    /** Número de FAEs por candidatura por omissão */
    private static final int FAES_POR_CANDIDATURA_POR_OMISSAO = 0;

    /**
     *
     * @param nomeAlgoritmo                Nome do algoritmos
     * @param faesPorCandidatura           FAEs por candidatura
     * @param evento                       Evento
     * @param organizador                  Organizador
     * @throws AlgorimoAtribuicaoException Excepção lançada da instanciação do algoritmo
     */
    public AbstractAlgoritmoAtribuicao(String nomeAlgoritmo, int faesPorCandidatura, Evento evento, Organizador organizador) throws AlgorimoAtribuicaoException {
        this.nomeAlgoritmo = nomeAlgoritmo;
        this.faesPorCandidatura = faesPorCandidatura;
        this.evento = evento;
        this.organizador = organizador;
        validaDadosExecucaoAlgoritmo();
    }

    /**
     *
     * @param nomeAlgoritmo                Nome do algoritmos
     * @param evento                       Evento
     * @param organizador                  Organizador
     * @throws AlgorimoAtribuicaoException Excepção lançada da instanciação do algoritmo
     */
    public AbstractAlgoritmoAtribuicao(String nomeAlgoritmo, Evento evento, Organizador organizador) {
        this.nomeAlgoritmo = nomeAlgoritmo;
        this.faesPorCandidatura = FAES_POR_CANDIDATURA_POR_OMISSAO;
        this.evento = evento;
        this.organizador = organizador;
        validaDadosExecucaoAlgoritmo();
    }

    /**
     *
     * @return Nome do algoritmo
     */
    public String getNomeAlgoritmo() {
        return nomeAlgoritmo;
    }

    /**
     *
     * @return FAESs por candidatura
     */
    public int getFaesPorCandidatura() {
        return faesPorCandidatura;
    }

    /**
     *
     * @return Evento
     */
    public Evento getEvento() {
        return evento;
    }

    /**
     *
     * @return Organizador
     */
    public Organizador getOrganizador() {
        return organizador;
    }

    /**
     *
     * @throws AlgorimoAtribuicaoException Lança excepção caso a validação falhe
     */
    public abstract void validaDadosExecucaoAlgoritmo() throws AlgorimoAtribuicaoException;

    /**
     * @param o O objecto a comparar
     * @return {@code true} se os objectos forem iguais, {@code false} caso contrário
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractAlgoritmoAtribuicao)) {
            return false;
        }
        AbstractAlgoritmoAtribuicao that = (AbstractAlgoritmoAtribuicao) o;
        return faesPorCandidatura == that.faesPorCandidatura &&
                Objects.equals(nomeAlgoritmo, that.nomeAlgoritmo) &&
                Objects.equals(evento, that.evento) &&
                Objects.equals(organizador, that.organizador);
    }

    /**
     * @return O hashCode do objecto
     */
    @Override
    public int hashCode() {
        return Objects.hash(nomeAlgoritmo, faesPorCandidatura, evento, organizador);
    }

    /**
     * @return Representação alfanumérica do objecto
     */
    @Override
    public String toString() {
        return String.format(
                "AbstractAlgoritmoAtribuicao [nomeAlgoritmo=%s, faesPorCandidatura=%s, evento=%s, organizador=%s]",
                this.nomeAlgoritmo,
                this.faesPorCandidatura,
                this.evento,
                this.organizador);
    }
}
