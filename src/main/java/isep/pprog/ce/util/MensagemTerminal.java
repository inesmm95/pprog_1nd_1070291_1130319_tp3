package isep.pprog.ce.util;

/**
 * Enumeração que contém as mensagens que a aplicação pode
 * escrever no contexto da interface com o utilizador.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public enum MensagemTerminal {

    UM("1 "),
    DOIS("2 "),
    TRES("3 "),
    OPCAO_INVALIDA("Opção inválida"),
    SELECCIONE_EVENTO("Seleccione um evento:"),

    BEM_VINDO_CENTRO_EVENTOS("Bem vindo ao Centro de Eventos! Seleccione uma das seguintes opções:"),
    LOGIN_COM_NOME_UTILIZADOR("Login com nome de utilizador:"),
    UTILIZADOR_NAO_EXISTE_INSIRA_NOV("O utilizador inserido não existe. Insira novamente"),
    INSIRA_A_ACCAO_PRETENDIDA("Seleccione a accão pretendida:"),
    ATRIBUIR_CANDIDATURA("1 - Atribuir Candidatura"),
    DECIDIR_CANDIDATURA("2 - Decidir Candidatura"),
    CRIAR_CANDIDATURA("3 - Criar Candidatura"),
    LOGOUT("0 - Logout"),
    SAIR("9 - Sair"),
    TIPO_UTILIZADOR_SEM_PERMISSAO("O tipo de utilizador não permite realizar a acção selecionada"),

    ACEITA_AS_ATRIBUICOES("Aceita as atribuiçoes 1 = Sim 0 = Não"),
    SELECCIONE_ALGORITMO_PRETENDIDO("Seleccione o algoritmo de atribuição pretendido:"),
    ALGORITMO_INVALIDO("Algoritmo inválido"),
    SELECCIONE_NR_FAES_CANDIDATURA("Seleccione o número de FAEs por candidatura"),
    OCORREU_UM_ERRO_NA_SELECCAO_ALGORITMO("Ocorreu um erro na selecção do algoritmo de atribuição"),
    EVENTO_NAO_TEM_FAES("O evento não tem Funcionários de Apoio ao Evento associados"),
    EVENTO_NAO_TEM_CANDIDATURAS("O evento não tem candidaturas"),
    EVENTO_NAO_EXISTE("O evento não existe"),
    ATRIBUICOES_GUARDADAS("Atribuições guardadas"),

    CONFIRMA_DECISAO("Confirma a sua decisão? 1 = Sim 0 = Não"),
    A_DECISAO_FOI_CONFIRMADA("A decisão foi confirmada"),
    A_DECISAO_NAO_FOI_CONFIRMADA("A decisão não foi confirmada"),
    NAO_TEM_EVENTOS_SELECCIONADOS("Não tem eventos associados"),
    NAO_POSSUI_CANDIDATURAS_PENDENTES("Não possui candidaturas a decidir pendentes de decisão"),
    SELECCIONE_UMA_CANDIDATURA("Seleccione uma candidatura"),
    A_CANDIDATURA_NAO_EXISTE("A candidatura não existe"),
    ACEITA_CANDIDATURA("Aceita a candidatura? Sim = 1 Não = 0"),
    JUSTIFIQUE_DECISAO("Justifique a sua decisão"),

    NAO_HA_EVENTOS_ABERTOS_A_CANDIDATURAS("Não há eventos abertos a candidaturas"),
    CONFIRMA_CRIACAO_CANDIDATURA("Confirma a criação da candidatura"),
    CANDIDATURA_SUBMETIDA_COM_SUCESSO("Candidatura submetida com sucesso"),
    CANDIDATURA_NAO_SUBMETIDA("Candidatura não submetida"),
    EVENTO_INVALIDO("Evento inválido"),
    INSIRA_DESCICAO_CANDIDATURA("Introduza a descrição da candidatura");

    private final String mensagem;

    MensagemTerminal(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }

    @Override
    public String toString() {
        return String.format("MensagemTerminal [mensagem=%s]", this.mensagem);
    }
}
