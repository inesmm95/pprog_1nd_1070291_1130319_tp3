package isep.pprog.ce.util;


import java.util.Scanner;

/**
 * Classe utilitária que contém as operações de entrada/saída
 * no contexto da interface com o utilizador.
 *
 * @author Raul Estrela
 * @author Inês Marques
 */
public final class InterfaceTerminalHelper {

    public static final int ZERO = 0;
    public static final int UM = 1;
    public static final int DOIS = 2;
    public static final int TRES = 3;

    private static final Scanner INPUT_SCANNER = new Scanner(System.in);

    private InterfaceTerminalHelper() {
    }

    public static void imprimir(String string) {
        System.out.println(string);
    }

    public static void imprimir(MensagemTerminal mensagemTerminal) {
        System.out.println(mensagemTerminal.getMensagem());
    }

    public static int lerInt() {
        int valorLido = INPUT_SCANNER.nextInt();
        limparInput();
        return valorLido;
    }

    public static String lerString() {
        return INPUT_SCANNER.nextLine();
    }

    private static void limparInput() {
        INPUT_SCANNER.nextLine();
    }

}
