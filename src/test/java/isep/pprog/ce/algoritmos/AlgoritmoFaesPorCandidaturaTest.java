package isep.pprog.ce.algoritmos;


import isep.pprog.ce.dominio.*;
import isep.pprog.ce.dominio.algoritmos.AlgoritmoFaesPorCandidatura;
import isep.pprog.ce.ex.AlgorimoAtribuicaoException;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AlgoritmoFaesPorCandidaturaTest {

    private AlgoritmoFaesPorCandidatura algoritmo;

    @Test(expected = AlgorimoAtribuicaoException.class)
    public void falhaValidacaoQuandoNaoPodeDistribuirEquitativamente() {

        Evento evento = new Congresso();

        evento.setFuncsApoioEvento(Arrays.asList(new FuncApoioEvento(), new FuncApoioEvento(), new FuncApoioEvento(), new FuncApoioEvento()));

        evento.setCandidaturas(Arrays.asList(new Candidatura(), new Candidatura()));

        Organizador organizador = new Organizador();

        algoritmo = new AlgoritmoFaesPorCandidatura(3, evento, organizador);
    }

    @Test
    public void atribuiCorrectamente() {

        Evento evento = new Congresso();

        Utilizador fae1 = new Utilizador();
        fae1.setNome("Inês Marques");
        fae1.setUsername("inesm");
        fae1.setEmail("inesm@mail.com");

        Utilizador fae2 = new Utilizador();
        fae2.setNome("António Rodrigues");
        fae2.setUsername("rodriguesa");
        fae2.setEmail("rodriguesa@email.com");

        FuncApoioEvento funcApoioEvento1 = new FuncApoioEvento(fae1, ExperienciaProfissional.JUNIOR);
        FuncApoioEvento funcApoioEvento2 = new FuncApoioEvento(fae2, ExperienciaProfissional.EXPERIENTE);

        evento.setFuncsApoioEvento(Arrays.asList(funcApoioEvento1, funcApoioEvento2));
        evento.setCandidaturas(
                Arrays.asList(new Candidatura(), new Candidatura())
        );

        algoritmo = new AlgoritmoFaesPorCandidatura(1, evento, new Organizador());

        List<Atribuicao> atribuicoes = algoritmo.atribui();

        assertEquals(2, atribuicoes.size());

        assertEquals(1, atribuicoes.stream().filter(a -> a.getFuncApoioEvento().equals(funcApoioEvento1)).count());

        assertEquals(1, atribuicoes.stream().filter(a -> a.getFuncApoioEvento().equals(funcApoioEvento2)).count());
    }
}
