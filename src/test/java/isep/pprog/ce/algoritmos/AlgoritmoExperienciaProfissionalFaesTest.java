package isep.pprog.ce.algoritmos;


import isep.pprog.ce.dominio.*;
import isep.pprog.ce.dominio.algoritmos.AlgoritmoExperienciaProfissionalFaes;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AlgoritmoExperienciaProfissionalFaesTest {

    @Test
    public void atribuiDeAcordoComExperience() {

        Evento evento = new Congresso();

        Utilizador fae1 = new Utilizador();
        fae1.setNome("Inês Marques");
        fae1.setUsername("inesm");
        fae1.setEmail("inesm@mail.com");

        Utilizador fae2 = new Utilizador();
        fae2.setNome("António Rodrigues");
        fae2.setUsername("rodriguesa");
        fae2.setEmail("rodriguesa@email.com");

        Utilizador fae3 = new Utilizador();
        fae3.setNome("Miguel Pereira");
        fae3.setUsername("pereiram");
        fae3.setEmail("pereiram@email.com");

        //14 candidatuas
        List<Candidatura> candidaturas = new ArrayList<>();
        candidaturas.add(new Candidatura());
        candidaturas.add(new Candidatura());
        candidaturas.add(new Candidatura());
        candidaturas.add(new Candidatura());
        candidaturas.add(new Candidatura());
        candidaturas.add(new Candidatura());
        candidaturas.add(new Candidatura());
        candidaturas.add(new Candidatura());
        candidaturas.add(new Candidatura());
        candidaturas.add(new Candidatura());
        candidaturas.add(new Candidatura());
        candidaturas.add(new Candidatura());
        candidaturas.add(new Candidatura());
        candidaturas.add(new Candidatura());

        evento.setCandidaturas(candidaturas);

        //deverá ficar com 20% das candidaturas - ie 3 candidaturas
        FuncApoioEvento funcApoioEvento1 = new FuncApoioEvento(fae1, ExperienciaProfissional.JUNIOR);

        //deverá ficar com 30% das candidaturas - ie 5 candidaturas
        FuncApoioEvento funcApoioEvento2 = new FuncApoioEvento(fae2, ExperienciaProfissional.EXPERIENTE);

        //deverá ficar com 40% das candidaturas - ie 6 candidaturas
        FuncApoioEvento funcApoioEvento3 = new FuncApoioEvento(fae3, ExperienciaProfissional.SENIOR);

        evento.setFuncsApoioEvento(Arrays.asList(funcApoioEvento1, funcApoioEvento2, funcApoioEvento3));

        AlgoritmoExperienciaProfissionalFaes algoritmo =
                new AlgoritmoExperienciaProfissionalFaes(evento, new Organizador());

        List<Atribuicao> atribuicoes = algoritmo.atribui();

        assertEquals(14, atribuicoes.size());

        assertEquals(3, atribuicoes.stream().filter(a -> a.getFuncApoioEvento().equals(funcApoioEvento1)).count());

        assertEquals(5, atribuicoes.stream().filter(a -> a.getFuncApoioEvento().equals(funcApoioEvento2)).count());

        assertEquals(6, atribuicoes.stream().filter(a -> a.getFuncApoioEvento().equals(funcApoioEvento3)).count());
    }
}
